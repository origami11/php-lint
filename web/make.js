﻿const v = require('../src/validate');
const { loadFile } = require('../src/lib/file');
const fs = require('fs');
var glob = require("glob");
const path = require('path');
const { scope } = require('../src/lib/scope');
const { getType } = require('../src/lib/print');
const { getMessages, isVisible, formatMessage } = require('../src/lib/messages');

var options = {
    scope: false,
    format: false,
    trace: false,
    message: 'all,-block',
    include: []
};

//var argv = process.argv.slice(2);
//if (argv.length > 0) {
var config = JSON.parse(fs.readFileSync('projects.json', 'utf-8'));

console.log(config);

var currentPath = config.basePath;
var filename = 'phplint.json';

if (fs.existsSync(currentPath + '/' + filename)) {
    var options = JSON.parse(fs.readFileSync(currentPath + '/' + filename, 'utf-8'));
    options.cwd = currentPath;
    options.tspath = currentPath;

    v.init(options);

    glob(options.files, { ignore: options.ignore, cwd: currentPath }, function (er, files) {
        files.forEach((f) => { 
            loadFile(path.join(currentPath, f));
        });

        var mx = Array.from(getMessages().entries());
        fs.writeFileSync('temp/output.json', JSON.stringify(mx));
    })
} else {
    console.log('not found ' + filename);
}
