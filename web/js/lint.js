﻿(function () {
    var 
        open = IncrementalDOM.elementOpen,
        close = IncrementalDOM.elementClose,
        empty = IncrementalDOM.elementVoid,
        text = IncrementalDOM.text,
        patch = IncrementalDOM.patch
    ;

function filename(f) {
    var p = f.lastIndexOf('\\');
    return f.substr(p + 1);
}

function onfile(item) {
    var path = 'http://localhost:8080/get.php?file=' + encodeURIComponent(item[0]);
    fetch(path).then(r => r.text()).then(txt => {
        console.log(item[1]);
        var root = document.getElementById('code');
        patch(root, renderLines, txt.split('\n'));        
    });
//    fetch();
}

function renderLines(list) {
    list.forEach((item, i) => {            
        open('div', null, ['class', 'line']);
            open('span', null, ['class', 'line-number']);
                text(i + 1); 
            close('span');           
            text(item);            
        close('div');
    })
}

function render(list) {
    list.forEach(item => {            
        open('div', null, ['class', 'file-item', 'onclick', onfile.bind(null, item)]);
            text(filename(item[0]) + ' [' + item[1].length + ']');
        close('div');
    })
}

fetch('temp/output.json').then(r => r.json()).then(list => {
    var root = document.getElementById('file-list');
    patch(root, render, list);        
}).catch( alert );

} ())