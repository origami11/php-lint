﻿//var reader = require('php-parser');
//reader.parser.locations = false;
//reader.parser.debug = true;
var fs = require('fs');

var code = require('fs').readFileSync(__dirname + '/tests/057.vartype.php', 'utf-8');
//var code = require('fs').readFileSync(__dirname + '/php-parser/test/proto/foo.php', 'utf-8');
//var code = require('fs').readFileSync(__dirname + '/php-parser/test/proto/test1.php', 'utf-8');

var parser = require('php-parser');
var reader = new parser({
  parser: {
    extractDoc: false,
    suppressErrors: false,
    php7: true
  },
  ast: {
    withPositions: true
  },
  lexer: {
    short_tags: true,
    asp_tags: true
  }
});

var AST = reader.parseCode(code);
console.log(AST.children);