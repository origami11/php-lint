<?php  

namespace ctiso\Test1 {

    class Cat {
        public $name;
    }

    class Dog {
        public $name;
    }
}


namespace ctiso\Test2 {
    use ctiso\Test1\Dog as Dog1;

    class Animal extends Dog1 {
        public $name;
    }

    class Animal3 extends \ctiso\Test1\Cat {
        function test1(Dog1 $x) {
            echo $x->name;
        }
    }


    $n = new Animal3();
}

