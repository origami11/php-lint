<?php
///<reference path="type.d.ts"/>

class X {
    function a() {}
}

class Y {
    function b() {}
}

$pos = strpos("x", "y");

if (is_int($pos)) {
    $x = $pos;
}

$u = new X();

if ($u instanceof X) {
    $u->a();    
}

if ($u instanceof Y) {
    $u->b();
}