<?php  

class Animal {
    public $cache = 0;
    static $instance = 1;

    function test() {
        echo self::$instance;
        echo $this->cache;
    }
}

$x = new Animal();
echo $x->cache;