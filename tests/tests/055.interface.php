<?php  
//declare(strict_types=1);

interface A {
    function fn1();
    function fn2();
    function fn3();
}

class B implements A {
    function fn1() {
        echo 1;
    }
    function fn2() {
        echo 2;
    }
}

class C {
}

function test(A $x) {
    $x->fn14();
}

test(new B());
