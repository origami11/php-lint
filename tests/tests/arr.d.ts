class ArrayAccess {
    function offsetExists($offset: any): boolean;
    function offsetGet($offset: any): any;
    function offsetSet($offset: any, value: any);
    function offsetUnset($offset: any);
}