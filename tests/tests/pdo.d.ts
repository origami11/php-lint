﻿
class PDOStatement<Result = array<string | number>> extends Iterator {
    function fetch($options?: number): Result;
    function fetchAll($fetch_style?: number, $fetch_argument?: any): list<Result>;
}

class PDO<Stmt = PDOStatement> {
    function prepare($statement: string<valid>, $options?: any): Stmt;
}
