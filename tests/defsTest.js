var PEG = require("pegjs");
var fs = require('fs');
var parser = PEG.buildParser(fs.readFileSync('peg/types.peg', 'utf-8'));

//var data = fs.readFileSync('runtime/core.d.ts', 'utf-8');
var data = fs.readFileSync('runtime/pdo.d.ts', 'utf-8');

var ast = parser.parse(data);
console.log(JSON.stringify(ast, null, 2));