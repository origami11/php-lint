﻿const v = require('../src/validate');
const { stat } = require('../src/lib/ast');
const m = require('../src/lib/messages');
const u = require('../src/lib/unify');
const { scope } = require('../src/lib/scope');
const { reset, loadFile } = require('../src/lib/file');

const path = require('path');
const cli = require('cli-color');

//var curFile = ['022.include.php', '023.encapsed.php'];
var curFile = ['057.vartype.php'];

v.init({runtime: false, stat: true, debug: true});

function make_class(name, parent, props) {
    return {type: 'class', name: name, parent: parent || false, property: props || {}};
}

function match_props(a, b) {  
}

var tests = {
    valid: 0,
    count: 0
};


function valid() {
    tests.valid ++;
    console.log(cli.green('ok'));    
}

function match_type(a, b) {
    if (a.type == 'var' && b.type != 'var') {
        return match_type(a.value, b);
    }

    if (a.type == 'class' && b.type == 'class') {
        return a.name == b.name && a.parent == b.parent;
    }
    return a.type == b.type;
}

function assertTrue(x, y) {
    if (x !== y) {
        console.log(x, 'should be', y);
    } else {
        valid();
    }    
    tests.count++;
}

function assertProperty(obj, prop, type) {
    var p = obj.property[prop];
    if (!match_type(p, type)) {
        console.log('types not match', type, obj.property[prop]);
    } else {
        valid();
    }
    tests.count++;
}

function assertMethod(obj, prop, args) {
    var m = obj.methods[prop];
    if (m.args.length != args.length) {
        console.log('args not match', args, m.args);
    } else {
        valid();
    }
    tests.count++;
}

function assertType(obj, type) {
    if (!match_type(obj, type)) {
        console.log('types not match', type, obj);
    } else {
        valid();
    }
    tests.count++;
}

function assertMessages(m_list, n) {
    n = n || [];
    if (m_list.length == n.length && m_list.every((a,i) => n[i].type == a.type)) {
        valid();
    } else {
        m_list.forEach(message => {
            console.log(m.formatMessage(currentFile, message));
        });
    }
    tests.count++;
}

var currentFile = null;
function testFile(file, fn) {
    var fname = path.basename(file);
    currentFile = file;

    if (curFile.length > 0 && curFile.indexOf(fname) < 0) {
        return;
    }

    console.log(file);

    reset();
    var fullname = loadFile(file);

    var m_list = m.getMessages();
    fn(m_list.get(fullname.toLowerCase()) || [], scope);
}

testFile('tests/001.assignment.php', function (m, s) {
    assertMessages(m);

    assertType(s.get_global('x'), {type: 'number'});
    assertType(s.get_global('y'), {type: 'string'});
    assertType(s.get_global('z'), {type: 'array'});
});

testFile('tests/002.classdef.php', function (m, s) {
    assertMessages(m);
    var a = s.get_global('Animal');

    assertType(a, make_class('Animal', false));
    assertProperty(a, 'name', {type: 'var', value: null});
    assertMethod(a, '__construct', []);
});


testFile('tests/003.new.php', function (m, s) {
    assertMessages(m);

    var a = s.get_global('Square');
    var sq = s.get_global('square');

    var type = make_class('Square', false);

    assertType(sq, type);
    assertType(a, type);

    assertProperty(a, 'size', {type: 'number'});
    assertMethod(a, '__construct', [{type: 'number'}]);
});


testFile('tests/004.staticfn.php', function (m, s) {
    assertMessages(m);

    var a = s.get_global('Math');
    var sm = s.get_global('sum');

    var type = make_class('Math', false);

    assertMethod(a, 'summa', [{type: 'number'}, {type: 'number'}]);
    assertType(sm, {type: 'number'});
});

testFile('tests/005.string.php', function (m, s) {
    assertMessages(m, [{'type': 'novar'}]);
//    assertTrue(m[0].type, 'type');

    assertType(s.get_global('x'), {type: 'string'});
});


testFile('tests/006.guard.php', function (m, s) {
    assertMessages(m);

    assertType(s.get_global('x'), {type: 'number'});
//    console.log(v.scope.get_global('pos'));
//    console.log(v.scope.get_global('x'));
});

testFile('tests/007.concat.php', function (m, s) {
    assertMessages(m);
});

false && testFile('tests/008.array.php', function (m, s) {
    assertMessages(m);
//    console.log(m);
});

false && testFile('tests/009.fs.php', function (m, s) {
    assertMessages(m);
//    console.log(v.scope.get_global('pathinfo'));
//    console.log(v.scope.get_global('$x'));
//    console.log(v.scope.get_global('$y'));
});

testFile('tests/010.fn.php', function (m, s) {
    assertMessages(m);
    assertType(s.get_global('z'), {type: 'number'});
});

false && testFile('tests/011.classfn.php', function (m, s) {
    assertMessages(m);
//    console.log(scope.get_global('X').methods.test);
});

testFile('tests/012.bin.php', function (m, s) {
    assertMessages(m);
    assertType(s.get_global('x'), {type: 'number'});
});

testFile('tests/013.foreach.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/014.math.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/015.inline.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/016.post.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/017.unary.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/018.cast.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/019.argtype.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/020.shortif.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/021.list.php', function (m, s) {
    assertMessages(m);
//    assertType(s.get_global('a'), {type: 'number'});
});

testFile('tests/022.include.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/023.encapsed.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/024.isset.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/025.throw.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/026.switch.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/027.for.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/028.while.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/029.unset.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/030.silent.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/031.magic.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/032.empty.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/033.exit.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/034.closure.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/035.super.php', function (m, s) {
    assertMessages(m);

    assertType(s.get_global('x'), {type: 'string'});
    assertType(s.get_global('z'), {type: 'number'});
});

testFile('tests/036.classconst.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/037.classprop.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/038.comment.php', function (m, s) {
    assertMessages(m);
    assertType(s.get_global('var'), {type: 'number'});
});

testFile('tests/039.global.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/040.try.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/041.ns.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/042.unifyprop.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/043.null.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/044.array.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/045.pre.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/046.do.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/047.vardef.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/048.defns.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/049.noparse.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/050.array2.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/051.ctype.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/052.override.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/053.self.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/054.static.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/055.interface.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/056.define.php', function (m, s) {
    assertMessages(m);
});

testFile('tests/057.vartype.php', function (m, s) {
    assertMessages(m);
    console.log(s.globals['d:\\server\\www\\projects\\php-lint\\tests\\tests\\pdo.d.ts']);
});

console.log(Math.round(tests.valid/tests.count * 100) + '%');
console.log(v.sysMods.length + '/' + Object.keys(stat).length, '=', Math.round(Object.keys(stat).length/v.sysMods.length * 100) + '%');

