﻿//var reader = require('php-parser');
//reader.parser.locations = false;
//reader.parser.debug = true;
var fs = require('fs');

var code = require('fs').readFileSync(__dirname + '/tests/038.comment.php', 'utf-8');
//var code = require('fs').readFileSync(__dirname + '/php-parser/test/proto/foo.php', 'utf-8');
//var code = require('fs').readFileSync(__dirname + '/php-parser/test/proto/test1.php', 'utf-8');

var parser = require('php-parser');
var reader = new parser({
  parser: {
    extractDoc: false,
    suppressErrors: true,
    php7: true
  },
  ast: {
    withPositions: true
  },
  lexer: {
    short_tags: true,
    asp_tags: true
  }
});

function isObject(o) {
  return o instanceof Object && typeof o == 'object';
}

function walk(ast, cc) {
    if (Array.isArray(ast)) {       
        ast.forEach(item => walk(item, cc));
    } else if (isObject(ast)) {
        cc(ast);
        for(var i in ast) {
            if (i != 'kind' && i != 'loc' && ast.hasOwnProperty(i)) {
                walk(ast[i], cc);
            }
        }
    }
}


var content = ['<style>@import url(style.css);</style><pre>'];
var last = 0;
var AST = reader.parseCode(code);
walk(AST, function (ast) {
    if (['variable', 'parameter', 'property'].indexOf(ast.kind) >= 0) {
        var loc = ast.loc;
        var begin = loc.start.offset
        var end = loc.end.offset;

        content.push(code.substring(last, begin));
        content.push('<span class="'+ast.kind+'">' + code.substring(begin, end) + "</span>");

        last = end;

//        console.log(ast.name, begin, end);
    }
});

content.push(code.substring(last));
content.push('</pre>');

console.log(content.join(''));
