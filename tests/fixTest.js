//var reader = require('php-parser');
//reader.parser.locations = false;
//reader.parser.debug = true;
var fs = require('fs');


var parser = require('php-parser');
var reader = new parser({
  parser: {
    extractDoc: false,
    suppressErrors: true,
    php7: true
  },
  ast: {
    withPositions: true
  },
  lexer: {
    short_tags: true,
    asp_tags: true
  }
});


var code = [
    "<?php /*abs*/",
    "    $x ? true : false "
].join("\n")
           

var AST = reader.parseCode(code);

var tokens = parser.tokenGetAll(code,   {ast: {
    withPositions: true
  }});

console.log(tokens);
console.log(AST);