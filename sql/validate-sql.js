﻿//
"use strict";

var nQuery = require('../lib/nquery');

function table_type(fields) {
    return {type: 'table', fields: fields};
}

function q(x) {
    return '"'+ x + '"';
}

class Scope {
    constructor() {
        this.hash = {}
    }

    set(name, type) {
        this.hash[name] = type;
    }

    get(name) {
        return this.hash[name];
    }

    find_column(name) {
        for(var i in this.hash) {            
            if (this.hash.hasOwnProperty(i)) {
                if (this.hash[i].fields.indexOf(name) >= 0) {
                    return true;
                }
            }
        }    
        return false;
    }
}

function validate_sql(query) {
    var scope = new Scope();    
    scope.set('users', table_type(['name', 'age']))

    var local = new Scope();
    
    var Parser = nQuery.Parser;
    var ast = Parser.parse(query);
    
    function accept_eval(x) {
        accept[x.type](x);
    }
    
    var accept = {
        'column_ref': function (ast) {
            var name = ast.column;
            if (!local.find_column(name)) {
                console.log('Unknown column ' + q(name));
            }
        },
        
       'select': function (ast) {
            ast.from.forEach(function (item) {
                var table = item.table;
                var result = scope.get(table);
                local.set(table, result);
            });
    
            ast.columns.forEach(function (col) {
                accept_eval(col.expr);
            });
        }
    }

    accept_eval(ast);
}

exports.validate_sql = validate_sql;
