﻿const v = require('./validate');
const { loadFile } = require('./lib/file');
const fs = require('fs');
var glob = require("glob");
const path = require('path');
const { scope } = require('./lib/scope');
const { getType } = require('./lib/print');
const cli = require('cli-color');
const { getMessages, isVisible, formatMessage, printMessages } = require('./lib/messages');

var options = {
    scope: false,
    format: false,
    trace: false,
    message: 'all,-block',
    include: []
};

function parseOptions(argv, options) {
    for(var i = 0; i < argv.length; i++) {
        if (argv[i].indexOf("--") == 0) {
            var option = argv[i].substr(2).split('=');
            if (option.length == 1) {
                options[option[0]] = true
            } else {
                if (option[0] == 'include') {
                    options[option[0]] = option[1].split(';');
                } else {
                    options[option[0]] = option[1];
                }
            }
        }
    }
}

function printAllScope(list, fn) {
    for(var i in list) {
        if (list.hasOwnProperty(i) && fn(i)) {
            console.log(cli.magenta(i + ":"));
            printScope(list[i]);
        }
    }
}

function printScope(list) {
    for(var i in list) {
        if (list.hasOwnProperty(i)) {
            console.log("    " + "var " + i + ": " + getType(list[i], true) + ";");
        }
    }
}

function isAbsolute(fp) {
  return fp.charAt(0) === '/';
};

//var argv = process.argv.slice(2);
//if (argv.length > 0) {
var currentPath = process.cwd();
var filename = 'phplint.json';

if (fs.existsSync(currentPath + '/' + filename)) {    
    var options = JSON.parse(fs.readFileSync(currentPath + '/' + filename, 'utf-8'));
    if (!options.cwd) {
        options.cwd = currentPath;
    }

    if (options.plugins) {
        options.plugins.forEach(plugin => require('./plugins/' + plugin));
    }

    v.init(options);

    glob(options.files, { ignore: options.ignore }, function (er, files) {
        files.forEach((f) => {
            loadFile(isAbsolute(f) ? f : path.join(options.cwd, f));
        });

        var mx = getMessages();
        if (options.format == 'json') {
            console.log(JSON.stringify(mx));
        } else {
            printMessages(mx);
        }
    
            if (options.scope == 'all') {
            printAllScope(scope.globals, (x) => true);
        } else if (options.scope == 'php') {
            printAllScope(scope.globals, (x) => path.extname(x) == '.php');
        } else if (options.scope) {
            console.log(cli.magenta(filename + ":"));
            printScope(scope.globals[filename]);
        }
    })
} else {
    console.log('not found ' + filename);
}
