﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_boolean } = require('../lib/types');
const { stack } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var args = code.arguments;

    args.forEach(function (arg) {
        accept_eval(arg);
        unify(arg.loc, t_any, stack.pop());
    });

    stack.push(t_boolean);
}
