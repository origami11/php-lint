﻿const { scope } = require('../lib/scope');
const { message } = require('../lib/messages');

module.exports = function (code) {    
    var result = scope.add_uses(code.name, code.alias);
    if (!result) {
        message(code.loc, 'usedup', []);
    }
}