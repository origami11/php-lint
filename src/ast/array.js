﻿const { accept_eval, array_ex } = require('../lib/ast');
const { t_any, t_number, t_array } = require('../lib/types');
const { unify } = require('../lib/unify');
const { stack } = require('../lib/scope');

module.exports = function (code) {
    var items = code.items;
    var arr = t_array();
    /* 
        считаем что по умолчанию все значения могут быть любого типа, а не по первому совпадению 
        в идеале нужно помнить типы для определенных полей массива!!
    */
    arr.value = t_any;

    if (items.length == 0) {
        arr.value = t_any;
    } else {
        items.forEach(function (item) {
            if (item.key) {
                accept_eval(item.key);
                unify(item.key.loc, arr.key, stack.pop());
            } else {
                unify(item.loc, t_number, arr.key);
            }

            accept_eval(item.value);
            var value = stack.pop();
            unify(item.value.loc, arr.value, value); 
        });
    }
    stack.push(arr);
};
