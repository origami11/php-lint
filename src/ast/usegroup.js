﻿const { accept_eval } = require('../lib/ast');
const { t_any } = require('../lib/types');
const { stack, class_scope } = require('../lib/unify');

module.exports = function (code) {
    code.items.forEach(c => {
        accept_eval(c);
    });
}