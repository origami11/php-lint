﻿const { t_boolean } = require('../lib/types');
const { stack } = require('../lib/scope');

module.exports = function(code) {
    stack.push(t_boolean);
};