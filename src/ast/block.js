﻿const { accept_stmt } = require('../lib/ast');

module.exports = function(code) {
    var stmt_list = code.children;
    stmt_list.forEach(accept_stmt);
};