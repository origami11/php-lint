﻿const { accept_eval } = require('../lib/ast');
const { t_void, t_var, t_case } = require('../lib/types');
const { stack, scope } = require('../lib/scope');
const { set_variable } = require('../lib/unify');
const { getPreloadedClass } = require('../lib/file');
const { message } = require('../lib/messages');

module.exports = function (code) {
    accept_eval(code.body);
    code.catches.forEach((item) => {
        var err = item.variable;
        var types = item.what.map((what) => {
            var name = scope.resolveNsName(what.name, what.resolution);
            var type = getPreloadedClass(name, what.loc);
            if (!type) {
                type = t_var();
                var altnames = scope.suggest(what.name);
                message(what.loc, 'noclassalt', [name, altnames.join(", ")]);
            }
            return type;
        });

        set_variable(err.name, types.length > 1 ? t_case(types) : types[0]);
        accept_eval(item.body);
    });
    stack.push(t_void);
}