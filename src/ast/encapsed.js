const { accept_eval } = require('../lib/ast');
const { t_case, t_number, t_string } = require('../lib/types');
const { stack } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var list = code.value;

    for(var i = 0; i < list.length; i++) {
        accept_eval(list[i]);
        var expr = stack.pop();        
        unify(list[i].loc, t_case([t_string(), t_number]), expr);
    }

    stack.push(t_string());
};