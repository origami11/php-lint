﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_void } = require('../lib/types');
const { stack } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var test = code.test;
    var body = code.body;

    accept_eval(body);

    accept_eval(test);
    unify(test.loc, t_any, stack.pop());

    stack.push(t_void);
};