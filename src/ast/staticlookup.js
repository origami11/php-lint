﻿
const { getPreloadedClass } = require('../lib/file');
const { get_type, accept_eval } = require('../lib/ast');
const { is_type, t_any, class_prop } = require('../lib/types');
const { message } = require('../lib/messages');
const { stack, class_scope, scope } = require('../lib/scope');

module.exports = function (code) {
    var name = code.what.name, tobj;

    if (!name) {
        message(code.loc, 'noclass', [name]);
        return;
    }

    if (name == 'self' || name == 'static') {
        tobj = class_scope.data;
    } else if (name == 'parent') {
        tobj = scope.get_global_ns(class_scope.data.parent);
    } else {
        var cname = scope.resolveNsName(name, code.what.resolution);
        tobj = getPreloadedClass(cname, code.what.loc);
    }

    if (is_type(tobj, 'class')) {        
        var getter = code.offset, gname;


        if (getter.kind == 'offsetlookup') {            
            // self::($x[1]) -> (self::$x)[1]
            code.offset = getter.what;
            getter.what = code;
            accept_eval(getter);
            return;
        }

        if (getter.kind == 'constref') {
            gname = getter.name;
        } else if (getter.kind == 'variable') {
            gname = getter.name;
        } else {
            gname = getter;
        }

        var type = class_prop(code.loc, tobj, gname);            
        if (type) {
            var ntype = get_type(type);
            if (ntype.type == 'function' && !ntype.is_static && !(['parent', 'self'].includes(name))) {
                message(getter.loc, 'notstatic', gname);
            }

            stack.push(type);
        } else {
            stack.push(t_any);
        }
        return;
    } else {
        message(code.loc, 'noclass', [name]);
    }

    stack.push(t_any);
};

