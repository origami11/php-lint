﻿const { accept_eval } = require('../lib/ast');
const { t_string, t_void } = require('../lib/types');
const { stack } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var args = code.arguments;

    if (Array.isArray(args)) {
        args.forEach(function(arg) {
            accept_eval(arg);
            unify(arg.loc, t_string(), stack.pop());
        });
    } else {
        accept_eval(args);
        unify(args.loc, t_string(), stack.pop());
    }
    stack.push(t_void);
}
