﻿const { t_string } = require('../lib/types');
const { stack } = require('../lib/scope');

module.exports = function (code) {
    stack.push(t_string('valid', code.value));
};