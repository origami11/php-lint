﻿const { accept_eval, is_node } = require('../lib/ast');
const { t_any, t_array, t_case, t_void, is_extends, get_class_prop } = require('../lib/types');
const { message } = require('../lib/messages');
const { unify, set_variable } = require('../lib/unify');
const { stack, scope } = require('../lib/scope');

module.exports = function (code) {
    var value = code.source;

    var key = code.key;
    var item = code.value;
    var body = code.body;

    accept_eval(value);
    var top = stack.pop();

    var riter = scope.get_global_ns(['Iterator']);

    var rarr = t_array();
    var key_type = rarr.key;
    var val_type = rarr.value;

    unify(value.loc, t_case([rarr, riter]), top);

    if (is_extends(top, 'Iterator')) {
        var fntype = get_class_prop(top, 'current');
        val_type = fntype.ret;  
    }
    
    if (key) {
        if (is_node(key, 'variable')) {
            set_variable(key.name, key_type); // string or number, array key
        } else {
            message(key.loc, '', 'Array key not variable');
        }
    }

    if (is_node(item, 'variable')) {
        set_variable(item.name, val_type); // any value, array value
    } else if (is_node(item, 'byref') && is_node(item.value, 'variable')) { 
        set_variable(item.value.name, val_type);
    } else {                
        message(item.loc, '', 'Array item not variable');
    }            

    accept_eval(item);
    unify(item.loc, stack.pop(), val_type);

    accept_eval(body);
    stack.push(t_void);
};
