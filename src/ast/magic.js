﻿const { t_any, t_string } = require('../lib/types');
const { message } = require('../lib/messages');
const { stack, scope } = require('../lib/scope');

module.exports = function (code) {
    if (code.value == '__DIR__' || code.value == '__FILE__') {
        stack.push(t_string('valid'));
        return; 
    }

    var obj = scope.get_global(code.value);
    if (!obj) {
        message(code.loc, 'noconst', [code.value]);
        stack.push(t_any);
    } else {
        stack.push(obj);
    }
};