﻿const { accept_eval } = require('../lib/ast');

module.exports = function(code) {
    accept_eval(code.inner);
};