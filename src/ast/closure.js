﻿const { accept_eval_type } = require('../lib/ast');

module.exports = function (code) {
    accept_eval_type('function', code, false);
};