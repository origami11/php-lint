
const { accept_eval } = require('../lib/ast');
const { t_any } = require('../lib/types');
const { message } = require('../lib/messages');
const { stack, scope, class_scope } = require('../lib/scope');

module.exports = function (code) {
    var name = code.name;

    if (typeof name != 'string') {
        accept_eval(name);
        return; 
    }

    if (name == 'this') {
        if (class_scope.data) {
            stack.push(class_scope.data);
        } else {
            stack.push(t_any);
            message(code.loc, 'nothis');
        }
        return;
    }

    var result = scope.get(name);

    if (result) {
        stack.push(result);
    } else {
        message(code.loc, 'novar', [code.name]);
        stack.push(t_any);
    }
};