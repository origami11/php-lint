﻿const { accept_eval, get_type } = require('../lib/ast');
const { is_type, t_any, t_string, t_number, t_case, t_array, t_var, has_interface } = require('../lib/types');
const { unify } = require('../lib/unify');
const { stack } = require('../lib/scope');
const { message } = require('../lib/messages');

module.exports = function (code) {
    var target = code.what, value = code.offset;

    accept_eval(target);
    var arr = get_type(stack.pop());

    // Уже знаем что это массив или строка
    if (has_interface(arr, 'ArrayAccess')) {
        if (value) {
            accept_eval(value);
            var index = stack.pop();

            unify(value.loc, t_case([t_string(), t_number]), index);
        }
        stack.push(t_any);
    } else if (is_type(arr, '\\array')) {
        // if value == false: $x[] == array_push($x, ...)
        if (value) {
            accept_eval(value);
            var index = stack.pop();

            unify(value.loc, arr.key, index);
        }
        stack.push(arr.value);
    } else if (is_type(arr, 'string')) {
        if (value) {
            accept_eval(value);
            var index = stack.pop();
            unify(value.loc, t_number, index);
        } else {
            message(code.loc, 'push', 'invalid string operation', []);
        }
        stack.push(t_string());
    } else {
        // Незнаем что за тип и считаем что массив
        var type = t_array(t_var(), t_var());

        if (value) {
            unify(value.loc, arr, type);

            accept_eval(value);
            var index = stack.pop();
            unify(value.loc, type.key, index);
        }
        stack.push(type.value);
    }
}
