﻿const { accept_eval } = require('../lib/ast');
const { t_any } = require('../lib/types');

module.exports = function (code) {
    accept_eval(code.expr);
}