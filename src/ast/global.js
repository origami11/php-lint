﻿const { t_void, t_var } = require('../lib/types');
const { stack, scope } = require('../lib/scope');

module.exports = function (code) {

    var list = code.items;
    list.forEach(function (item) {
        if (Array.isArray(item)) {
            // static function variable
            var name = item[0];
            scope.set(name, t_var());
        } else {
            var name = item.name;
            var obj = scope.get_global(name);
            if (obj) {
                scope.set(name, obj);
            } else {
                scope.set_global(name, t_var());
                //message(item.loc, 'novar', [name]);
            }
        }
    });
    stack.push(t_void);
}
