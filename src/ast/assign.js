﻿const { ast_type, accept_eval } = require('../lib/ast');
const { t_var, t_array, t_any } = require('../lib/types');
const { stack, scope } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { getCommentType } = require('../lib/file');

module.exports = function (code) {

    function assignType(name, type) {
        if (scope.get(name)) {
            unify(target.loc, type, scope.get(name));
        } else {
            scope.set(name, type);
        }
    }

    var target = code.left;
    if (ast_type(target) == 'list') {

        accept_eval(code.right);
        var expr = stack.pop();
        var val_type = t_var();

        var arr_type = t_array(t_any, val_type);
        unify(code.right.loc, arr_type, expr);

        var args = target.arguments;
        args.forEach(function (arg) {
            if (arg) {
                if (ast_type(arg) == 'variable') {
                    var name = arg.name;
        
                    if (!scope.get(name)) {
                        scope.set(name, t_var());
                    } 
                    unify(arg.loc, val_type, scope.get(name));
                } else {            
                    accept_eval(arg);
                    unify(arg.loc, val_type, stack.pop());
                }
            }
        });

    } else if (ast_type(target) == 'variable') {
        var name = target.name, varType = getCommentType(target, target.loc);
        if (varType) {
            assignType(name, varType);
        }

        accept_eval(code.right);
        var expr = stack.pop();        

        assignType(name, expr);
        stack.push(scope.get(name));
        return;
    } else {
        accept_eval(target);
        var result = stack.pop();

        accept_eval(code.right);
        var expr = stack.pop();

        unify(target.loc, result, expr);
        stack.push(result);
    }
};
