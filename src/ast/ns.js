﻿const { variable } = require('../lib/ast');
const { t_any } = require('../lib/types');
const { stack, class_scope } = require('../lib/unify');

module.exports = function (code) {
    var path = ns.args(code);
    var result = scope.get_global_ns(path);
    if (result) {
        stack.push(result);
    } else {
        message(code.loc, 'noname', [path.join("\\")]);
        stack.push(t_any);
    }
}