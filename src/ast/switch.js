﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_void } = require('../lib/types');
const { stack, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {

    var test = code.test;
    accept_eval(test);            
    var expr = stack.pop();

    var list = code.body.children;
            
    list.forEach(function (cond) {

        if (cond.test) {
            accept_eval(cond.test);
            unify(cond.test.loc, expr, stack.pop());
        }

        cond.body && accept_eval(cond.body);
    })

    stack.push(t_void);
};