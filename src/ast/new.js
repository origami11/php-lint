﻿const { is_name_ns, args_count, accept_eval } = require('../lib/ast');
const { is_type, t_any, t_class, get_class_prop } = require('../lib/types');
const { stack, scope, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { message } = require('../lib/messages');
const { getPreloadedClass } = require('../lib/file');

module.exports = function (code) {
    var what = code.what;

    if (is_name_ns(what)) {
        var name = scope.resolveNsName(what.name, what.resolution);        
        var obj = getPreloadedClass(name, code.loc);

        if (!obj) {                
            obj = t_class(name, false); // Пустой класс
            message(code.loc, 'noclass', [name]);
        } else {
            var fntype = get_class_prop(obj, '__construct');
           
            if (fntype && is_type(fntype, 'function')) {
                var args = code.arguments;
                var n_args = args_count(fntype.args);

                if (args.length < n_args.min || args.length > n_args.max) {
                    message(code.loc, 'noargs', [fntype.name, name]);
                }

                args.forEach(function(arg, i) {
                    accept_eval(arg);
                    var expr = stack.pop();

                    if (fntype.args.length > i) {
                        unify(arg.loc, fntype.args[i].type, expr);
                    }
                });
            }
        }

        stack.push(obj);
    } else {
        stack.push(scope.get_global_ns(['stdClass']));
    }
};
