﻿const { accept_stmt } = require('../lib/ast');
const { t_any } = require('../lib/types');
const { scope } = require('../lib/scope');

module.exports = function (code) {
    scope.begin_ns(code.name);
    var stmt_list = code.children;
    stmt_list.forEach(accept_stmt);
    scope.end_ns();
}