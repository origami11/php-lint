﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_void, t_boolean, t_number, t_string, t_case } = require('../lib/types');
const { stack, scope, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { options } = require('../lib/options');
const { message } = require('../lib/messages');
const { getPreloadedClass } = require('../lib/file');

module.exports = function (code) {             
    var test = code.test;

    var tmp = null, name = null;
    var guard_map = {
        'is_int': t_number,
        'is_bool': t_boolean,
        'is_string': t_string(),
        'is_resource': scope.get_global('PhpResource')
    };

    // simple type guards
    if ((test.kind == 'bin' && test.type == '!==') && 
        (test.left.kind == 'variable' && test.right.kind == 'boolean')) {

        name = test.left.name;
        tmp = scope.get(name);

        if (tmp.type == 'case') {
            var local = tmp.list.filter(function (t) {
                if (t.type == 'boolean' && t.value == 'false' && test.right.value == false) {
                    return false;
                }    
                return true;
            })
        
            scope.set(name, (local.length == 1) ? local[0] : t_case(local));
        }

        stack.push(t_boolean);
    } else if ((test.kind == 'bin' && test.type == 'instanceof') && 
        (test.left.kind == 'variable' && test.right.kind == 'constref')) {
        name = test.left.name;

        tmp = scope.get(name);

        var constRef = test.right.name;
        var className = scope.resolveNsName(constRef.name, constRef.resolution);
        var type = getPreloadedClass(className, constRef.loc);

//        console.log(type.name, tmp.name);

        if (type) {
            scope.set(name, type);
        } else {
            message(constRef.loc, 'noclass', className);
        }

        stack.push(t_boolean);
    } else if (test.kind == 'call' && test.what.kind == 'identifier') {
        var fn = test.what.name;
        var val = test.arguments.length == 1 && test.arguments[0].kind == 'variable';
        if (fn in guard_map && val) {
            name = test.arguments[0].name;
            tmp = scope.get(name);

            var type = guard_map[fn];

            unify(test.loc, tmp, type);
            scope.set(name, type);
        }

        stack.push(t_boolean);
    } else {
        accept_eval(test);
    }

    var rtest = stack.pop();

    if (options.strict) {
        unify(test.loc, t_case([t_boolean, t_number, t_string]), rtest);
    } else {
        unify(test.loc, t_any, rtest);
    }

    var b_then = code.body;

    accept_eval(b_then);
    if (name && tmp) {
        scope.set(name, tmp);
    }

    var b_else = code.alternate;
    if (b_else) {
        accept_eval(b_else);
    }
    stack.push(t_void);
};
