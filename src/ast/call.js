﻿const { accept_eval, get_type, args_count } = require('../lib/ast');
const { t_any, t_string, t_void, is_type } = require('../lib/types');
const { message } = require('../lib/messages');
const { stack, scope, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { options } = require('../lib/options');
const { getType } = require('../lib/print');

module.exports = function (code) {

    var fn = code.what;
    var args = code.arguments;

    var input = args.map(function(arg, i) {
        accept_eval(arg);
        return {loc: arg.loc, expr: stack.pop()};
    });

    if (fn.kind == 'identifier' && fn.name == "debug" && options.debug) {
        input.forEach(x => {
            var pos = code.loc.start;
            console.log('DEBUG:', scope.file + '(' + pos.line + ',' + pos.column + ')', getType(x.expr, true, 2));
        });
        stack.push(t_any);
        return;
    }

    if (fn.kind == 'identifier' && fn.name == "define") {
        var first = input[0];
        var second = input[1];

        unify(first.loc, t_string(), first.expr);
        unify(second.loc, t_any, second.expr);

        if (args[0].kind == 'string') {
            scope.set_global(args[0].value, second.expr);
        }

        stack.push(t_void);
        return;
    }

    accept_eval(fn);
    var result = stack.pop();

    var fntype = get_type(result);

    if (is_type(fntype, 'function')) {
        var next = false;
        do {
            next = false;
            var n_args = args_count(fntype.args);
            if (args.length < n_args.min || args.length > n_args.max) {
                if (fntype.hasOwnProperty('override') && fntype.override) {
                    fntype = fntype.override;
                    next = true;
                    continue;
                } else {
                    message(fn.loc, 'noargs', [fntype.name]);
                }
            }

            input.forEach(function(arg, i) {
                if (fntype.args.length > i) {
                    var arg_type = get_type(fntype.args[i].type);
                    unify(arg.loc, arg_type, arg.expr);
                }
            });
        } while(next);

        var ret_type = get_type(fntype.ret);
        stack.push(ret_type);
    } else {
        stack.push(t_any)
    }
};
