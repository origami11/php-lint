﻿const { accept_eval } = require('../lib/ast');
const { t_number } = require('../lib/types');
const { unify } = require('../lib/unify');
const { stack } = require('../lib/scope');

module.exports = function (code) {        
    var op = code.type;
    if (op == "+" || op == "-") {
        var arg1 = code.what;
        accept_eval(arg1);
        unify(arg1, t_number, stack.pop());

        stack.push(t_number);
    } else {
        throw new Error('Unknown postfix operator ' + op);
    }
}