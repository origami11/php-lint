﻿const { accept_eval, get_type, is_node } = require('../lib/ast');
const { is_type, t_void, t_any, t_class, t_string, t_func, get_class_prop, get_interface_prop } = require('../lib/types');
const { stack, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { message } = require('../lib/messages');

module.exports = function (prop) {
    var type = t_class(undefined, false); // Неопределенный класс
    accept_eval(prop.what);

    var obj = stack.pop();

    // До унификации может быть t_var
    unify(prop.what.loc, type, obj);
    var tobj = get_type(obj);

    // console.log(tobj);

    if (is_type(tobj, 'interface')) {
        if (is_node(prop.offset, 'constref')) {
            var name = prop.offset.name;
            var type = get_interface_prop(tobj, name);
            if (type) {
                stack.push(type);
            } else {
                message(prop.offset.loc, 'noprop', [name, tobj.name]);
                stack.push(t_any);
            }
        }
    }

    if (is_type(tobj, 'class')) {
        if (is_node(prop.offset, 'constref')) {
            var name = prop.offset.name;
            var type = get_class_prop(tobj, name);
            if (!type) {
                var setter = get_class_prop(tobj, "__set");
                if (setter) {
                    var fnargs = [{type: t_string(), name: 'key', optional: false}, {type: t_any, name: 'value', optional: false}];
                    unify(prop.offset.loc, setter, t_func('__set', t_void, fnargs));
                    type = t_any;
                } else {
                    message(prop.offset.loc, 'noprop', [name, tobj.name]);
                }
            }            

            if (type) {
                stack.push(type);
            } else {
                stack.push(t_any);
            }
            return;
        }
    }

    stack.push(t_any);
}            