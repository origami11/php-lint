﻿const { variable } = require('../lib/ast');
const { t_any, t_factory } = require('../lib/types');
const { stack, scope } = require('../lib/scope');
const { message } = require('../lib/messages');

module.exports = function (code) {  

    var name = code.name.name;
    var obj = scope.get_global(name);

    if (name == 'null') {
        obj = t_any;
    } else if (!obj) {
        obj = t_factory(name);
        message(code.loc, 'noconst', [name]);
    }

    stack.push(obj);
}