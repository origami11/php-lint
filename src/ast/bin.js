﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_boolean, t_string, t_number, t_case } = require('../lib/types');
const { unify, unify_types } = require('../lib/unify');
const { scope, stack } = require('../lib/scope');

const util = require('util');

module.exports = function (code) {   
    var op = code.type;
    var arg1 = code.left, arg2 = code.right;

    if (['+', '-', '*', '/', '&', '%', '|', '>>', '<<', '^'].indexOf(op) >= 0) {
        accept_eval(arg1);
        unify(arg1.loc, t_number, stack.pop());

        accept_eval(arg2);
        unify(arg2.loc, t_number, stack.pop());

        stack.push(t_number);
    } else if (op == ".") {

        //if (arg1.type == 'const' && arg1.value == "sql" && arg2.type == 'string') {
            //var validate_sql = require('./validate-sql').validate_sql;
            //validate_sql(arg2.value);
        //}            

        accept_eval(arg1);
        var left = stack.pop();

        accept_eval(arg2);
        var right = stack.pop();

        unify(arg1.loc, t_case([t_string(), t_number]), left);
        unify(arg2.loc, t_case([t_string(), t_number]), right);

        var valid = t_string('valid');
        if (unify_types(valid, left) && unify_types(valid, right)) {
            stack.push(valid);
            return;
        }

        stack.push(t_string());
    } else if (op == 'instanceof') {
        accept_eval(arg1);
        unify(arg1.loc, t_any, stack.pop());

        accept_eval(arg2);
        unify(arg2.loc, t_any, stack.pop());

        stack.push(t_boolean);
    } else if (['==', '!=', '===', '!=='].indexOf(op) >= 0) {

        accept_eval(arg1);
        var r1 = stack.pop();
        accept_eval(arg2);
        var r2 = stack.pop();

        unify(arg2.loc, r1, r2);
        stack.push(t_boolean);

    } else if (['>', '<', '>=', '<=', '<=>'].indexOf(op) >= 0) {
        accept_eval(arg1);
        unify(arg1.loc, t_number, stack.pop());

        accept_eval(arg2);
        unify(arg2.loc, t_number, stack.pop());

        stack.push(t_boolean);
    } else if (['&&', '||', 'or', 'and', '??'].indexOf(op) >= 0) {
        accept_eval(arg1);
        unify(arg1.loc, t_any, stack.pop()); // Булевы операции в приципе могут быть с любыми типами в php

        accept_eval(arg2);
        unify(arg2.loc, t_any, stack.pop()); // Булевы операции в приципе могут быть с любыми типами в php

        stack.push(t_boolean);
    } else {
        throw new Error(util.format('Unknown binary operator "%s"', op));
    }
};
