﻿const { accept_eval } = require('../lib/ast');
const { t_void } = require('../lib/types');
const { stack, scope } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { message } = require('../lib/messages');

module.exports = function (code) {
    var value = code.expr;
    if (value) {
        // for  non empty return
        accept_eval(value);      
        var top = scope.top();
        if (top) {
            top.fn.hasret = true;
            unify(value.loc, scope.top().fn.ret, stack.pop());
        } else {
            message(code.loc, 'noret');
        }
    }
    stack.push(t_void);
}