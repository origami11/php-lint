﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_boolean, t_void, t_number } = require('../lib/types');
const { stack } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var init = code.init;
    var test = code.test;
    var next = code.increment;
    var body = code.body;

    if (init) {
        init.forEach(function (x) {
            accept_eval(x);
        });
    }

    test.forEach(function (x) {
        accept_eval(x);
        unify(x.loc, t_boolean, stack.pop());
    });

    if (next) {
        next.forEach(function (x) {
            accept_eval(x);   
            var result = stack.pop();
            unify(x.loc, t_number, result);
        });
    }

    accept_eval(body);
    stack.push(t_void);
}
