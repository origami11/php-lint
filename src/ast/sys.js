﻿const { variable } = require('../lib/ast');
const { t_any } = require('../lib/types');
const { stack, class_scope } = require('../lib/unify');

module.exports = function (code) {
    var fn = sys.fn(code);
    var args = sys.args(code);

    if (fn == 'print') {
        accept_eval(args);
        unify(args.loc, t_string(), stack.pop());

        stack.push(t_void);
        return;
    }
}
