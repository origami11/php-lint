﻿const { accept_eval } = require('../lib/ast');
const { t_void } = require('../lib/types');
const { stack, class_scope } = require('../lib/scope');

module.exports = function (code) {
    accept_eval(code.what);
    stack.pop();
    stack.push(t_void);
}
