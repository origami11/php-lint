﻿const { accept_eval } = require('../lib/ast');
const { t_string, t_any } = require('../lib/types');
const { stack } = require('../lib/scope');
const { unify } = require('../lib/unify');
const { resolveFilePath, loadFromFile, loadDefs } = require('../lib/file');

const path = require('path');

module.exports = function (code) {
    var args = code.target;

    if (args.kind == 'string') {
        var pathName = resolveFilePath(args.value, args.loc);
        if (pathName) {
            if (path.extname(pathName) == '.php') {
                loadFromFile(pathName);
            } else {
                loadDefs(pathName, 1);
            }
        }                
    }

    accept_eval(args);
    unify(args.loc, t_any, stack.pop());

    stack.push(t_any);
}
