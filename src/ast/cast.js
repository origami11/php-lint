﻿const { accept_eval } = require('../lib/ast');
const { t_string, t_number, t_array, t_any, t_boolean } = require('../lib/types');
const { stack, class_scope, scope } = require('../lib/scope');

module.exports = function (code) {
    accept_eval(code.what);
    stack.pop();
    var name = code.type;
    if (name == 'string') {
        stack.push(t_string());
        return;
    }
    if (name == 'double' || name == 'int' || name == 'float') {
        stack.push(t_number);
        return;
    }
    if (name == 'boolean' || name == 'bool') {
        stack.push(t_boolean);
        return;
    }
    if (name == 'array') {
        stack.push(t_array(t_any, t_any));
        return;
    }
    if (name == 'object') {
        stack.push(scope.get_global('stdClass'));
        return;
    }
    throw new Error('Unknown cast ' + name);
};
