﻿const { isLowerCamelCase, accept_eval } = require('../lib/ast');
const { t_any, t_void, t_func } = require('../lib/types');
const { unify } = require('../lib/unify');
const { stack, scope, class_scope } = require('../lib/scope');

const { message } = require('../lib/messages');
const { getArgumentType } = require('../lib/file');

module.exports = function (code, is_method) {    
    var name = code.name;
    var args = code.arguments;
    var use = typeof code.uses == 'undefined' ? [] : code.uses;

    var result;

    if (is_method) {
        if (name && !isLowerCamelCase(name)) {
            message(code.loc, 'fnname', [name]);
        }
        result = class_scope.get_method(name);
    } else {            
        result = t_func(name);
        if (name) {
            // Не анонимная функция
            scope.set_global(name, result);
        }
    }

    scope.begin();
    scope.top().fn = result;

    if (use) {
        use.forEach(function (arg, i) {
            result.use.push(arg.name);
        });
    }        

    args.forEach(function (arg, i) {
        var name = arg.name;
    
        var type = getArgumentType(arg);
        var optional = false;

        if (i < result.args.length) {
            unify(arg.loc, result.args[i].type, type);
        } else {
            if (name == '_rest') {
                // Ugly Hack
                Array.apply(null, {length: 10}).forEach((x, i) => {
                    result.args.push({type: t_any, name: name + i, optional: true})
                });
                optional = true;
            } else {
                optional = arg.value !== null;
                result.args.push({type: type, name: name, optional: optional});
            }
        }
        scope.set(name, type, true);
    });

    accept_eval(code.body);

    if (!result.hasret) {
        unify(null, t_void, result.ret);
    }

    var fnscope = scope.top();
    Object.keys(fnscope.uses).forEach(function (key) {
        if (fnscope.uses[key] == 0 && result.args) {
            message(code.loc, 'unused', [key]);
        }
    });

    scope.end();
    stack.push(result);
}
