﻿const { isUpperCamelCase, is_node, accept_eval_type, accept_eval } = require('../lib/ast');
const { t_any, t_class, t_var, t_func, t_void, parent_props } = require('../lib/types');

const { stack, scope, class_scope } = require('../lib/scope');
const { message } = require('../lib/messages');
const { unify } = require('../lib/unify');

const { getType } = require('../lib/print');

const { getCommentType, getPreloadedClass } = require('../lib/file');

function accept_property(prop) {
    var c_type = getCommentType(prop, prop.loc);

    var var_type = t_var();
    if (prop.static) {
        accept_eval(prop.static);
        var_type = stack.pop();
    }
    
    if (c_type) {
        unify(prop.loc, c_type, var_type);
    }

    class_scope.set_prop(prop.name, var_type);
}

function has_method(list, m) {
    return list.find(method => method.name == m);
}

function validate_interface(loc, interfaces, methods) {
    interfaces.forEach((name) => {
        var item = scope.get_global(name);        
        for(var m in item.methods) {
            if (item.methods.hasOwnProperty(m) && !has_method(methods, m)) {
                message(loc, 'noimethod', [m]);
            }
        }
    });
}

module.exports = function (code) {

    var name = code.name;

    if (scope.ns.uses.hasOwnProperty(name)) {
        message(code.loc, 'classuse', name);
    }

    if (scope.ns.name) {
        name = scope.ns.name + '\\' + name;
    }

    var className = name;

    var body = code.body;

    var type = t_class(name, true);
    type.loc = {file: scope.file};
    scope.set_global(name, type);

    // Проверить класс
    var parent = code.extends;
    if (!isUpperCamelCase(name)) {
        message(code.loc, 'classname', [name]);
    }

    if (parent) {       
        var cname = scope.resolveNsName(parent.name, parent.resolution);
        var tobj = getPreloadedClass(cname, code.loc);

        if (!tobj) {
            message(code.loc, 'noclass', [cname]);
        }

        type.parent = cname;
    }

    code.implements && code.implements.forEach(function (arg) {
        var cname = scope.resolveNsName(arg.name, arg.resolution);
        var tobj = getPreloadedClass(cname, arg.loc);

        if (!tobj) {
            message(arg.loc, 'noclass', [cname]);
        }

        type.implement.push(cname);
    });

    class_scope.begin(type);

    var constants = body.filter(x => x.kind == 'classconstant');
    var properties = body.filter(x => x.kind == 'property');
    var methods = body.filter(x => x.kind == 'method');

    constants.forEach(function (item) {
        var var_type = t_var();
        if (item.value) {
            accept_eval(item.value);
            var_type = stack.pop();
        }
        class_scope.set_const(item.name, var_type);
    });

    properties.forEach(function (item) {
        accept_property(item);
    });

    // Сначала просматриваем все свойства
    methods.forEach(function (item) {
        var name = item.name;
        var args = item.arguments;

        if (name.toLowerCase() == className.toLowerCase()) {
            message(item.loc, 'badconstr', [name]);
        }

        var result = t_func(name);
        args.forEach(function (arg) {
            var name = arg.name;
            var type = t_var();
            if (name == '_rest') {
                // Ugly Hack
                Array.apply(null, {length: 10}).forEach((x, i) => {
                    result.args.push({type: t_any, name: name + i, optional: true})
                });
            } else {
                result.args.push({type: type, name: name, optional: arg.value !== null});
            }
        });
        result.is_static = item.isStatic;

        if (name != '__construct') {
            var parentFn = parent_props(type.parent, name);
            if (parentFn) {
                if (parentFn.args.length != result.args.length) {
                    message(item.loc, 'override', [getType(result, false), getType(parentFn, false)]);
                }
            }
        }

        class_scope.set_method(name, result);
    });

    methods.forEach(function (item) {
        accept_eval_type('function', item, true);
    });

    validate_interface(code.loc, type.implement, methods);
    
    class_scope.end();
    stack.push(t_void);
}
