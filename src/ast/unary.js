﻿const { accept_eval } = require('../lib/ast');
const { t_any, t_number, t_boolean } = require('../lib/types');
const { stack, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var op = code.type;
    if (op == "!") {
        var arg1 = code.what;
        accept_eval(arg1);
        unify(arg1.loc, t_any, stack.pop()); // В php отрицание может быть для любого типа
        stack.push(t_boolean);
    } else if (op == "-" || op == "+" || op == '~') {
        var arg1 = code.what;
        accept_eval(arg1);
        unify(arg1.loc, t_number, stack.pop());
        stack.push(t_number);
    } else {
        throw new Error('Unknown unary operator ' + op);
    }
}
