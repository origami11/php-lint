﻿const { accept_eval } = require('../lib/ast');
const { t_any } = require('../lib/types');
const { stack, class_scope } = require('../lib/scope');
const { unify } = require('../lib/unify');

module.exports = function (code) {
    var test = code.test;
    accept_eval(test);
    unify(test.loc, t_any, stack.pop());

    var b_then = code.trueExpr;

    if (b_then) {
        accept_eval(b_then);
        var expr1 = stack.pop();
    }

    var b_else = code.falseExpr;
    accept_eval(b_else);
    var expr2 = stack.pop();

    if (b_then) {
        unify(b_then.loc, expr1, expr2);
        stack.push(expr1);
    } else {
        stack.push(expr2);
    }
}