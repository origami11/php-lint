
const { t_any } = require('../lib/types');
const { message } = require('../lib/messages');
const { stack, scope } = require('../lib/scope');

module.exports = function (code) {
    var name = code.name;
    var result = scope.get_global(name);    

    if (result) {
        stack.push(result);
    } else {
        message(code.loc, 'noid', [name]);
        stack.push(t_any);
    }
};