"use strict";                                             

const fs = require('fs');
const path = require('path');
const cli = require('cli-color');
const md5 = require('md5');

const { add_module } = require('./lib/ast');
const { options } = require('./lib/options');
const { loadDefs } = require('./lib/file');

var sysDefs = [
    'core', 'spl', 'array', 'pdo', 'ctype', 'pgsql', 'math',
    'array', 'datetime', 'strings', 'stream', 'pcre', 'url',
    'exec', 'funchand', 'filesystem', 'mail', 'reflection',
    'classobj', 'json', 'filter', 'var', 'dir', 'info',
    'dom', 'curl', 'image', 'simplexml', 'errorfunc',
    'misc'];

var sysMods = [
    'array', 'assign', 'bin', 'break', 'byref', 'call', 'block', 'echo', 'boolean', 'inline',
    'cast', 'class', 'constref', 'continue', 'do', 'for', 'foreach', 'parenthesis', 'closure',
    'function', 'global', 'if', 'link', 'magic', 'new', 'include', 'encapsed', 'isset', 'unset',
    'ns', 'number', 'offsetlookup', 'post', 'program', 'propertylookup', 'retif', 'empty', 'exit', 
    'return', 'silent', 'staticlookup', 'string', 'switch', 'throw', 'null', 'pre', 'namespace', 
    'interface', 'try', 'unary', 'usegroup', 'useitem', 'variable', 'identifier', 'while'];


function loadDefaultDefs() {
    sysDefs.forEach(function (def) {
        loadDefs(path.join(__dirname, '../runtime', def + '.d.ts'), 2);
    })
}

function loadModules() {
    sysMods.forEach((name) => {
        var fn = require(__dirname + '/ast/' + name + '.js');
        add_module(name, fn);
    });
}    

function init(opts) {
    if (opts) {
        for(var i in opts) {
            if (opts.hasOwnProperty(i)) {
                options[i] = opts[i];
            }
        }
    }

    loadModules();

    if (options.runtime) {
        loadDefaultDefs();
    }

    options.include.forEach(item => {
        loadDefs(path.join(options.tspath, item), 1);
    });
}

exports.init = init;
exports.sysMods = sysMods;
