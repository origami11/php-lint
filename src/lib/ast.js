//

const { message } = require('./messages');
const { options } = require('./options');
const { scope, stack } = require('./scope');
const util = require('util');
const path = require('path');
const fs = require('fs');

var accept = {};
var before = new Map();

var plugin = {
    before: function (name, callback) {
        if (!before.has(name)) {
            before.set(name, []);
        }
        before.get(name).push(callback);
    }
};

function ast_type(code) {
    return code.kind;
}

function is_node(node, type) {
    return (node.kind == type);
}

function is_name_ns(name) {
    return name.kind == 'identifier';
}

var stat = {};
function accept_eval(code) {    
    if (code && accept.hasOwnProperty(ast_type(code))) {
//        if (options.trace) {
//            console.log(scope.file, code.position.start); // add --trace option
//        }
        if (options.stat) {
            stat[code.kind] = 1;            
        }
        var kind = code.kind;
        if (before.has(kind)) {
            before.get(kind).forEach(fn => fn(code));
        }        
        accept[kind](code);
        return;
    }
    if (code) {
        message(code.loc, 'ast', [ast_type(code)]);
    } else {
        throw new Error('Unknown ast');
    }
}

function accept_block(body) {
    if (Array.isArray(body)) {
        body.forEach(accept_stmt);
    } else {
        accept_stmt(body);
        message(body.position, 'block', []);
    }
}

function accept_stmt(code) {
    if (code) {
        accept_eval(code);
        stack.pop();
    }
}

function isLowerCamelCase(str) {
    return str.match(/^_*[a-z][A-Za-z0-9]+$/);
}

function isUpperCamelCase(str) {
    return str.match(/^[A-Z][A-Za-z0-9]+(_[A-Z][A-Za-z0-9]+)*$/);
}

function args_count(args) {
    var a_max = 0, a_min = 0;
    for(var i = 0; i < args.length; i++) {
        if (args[i].optional == false) {
            a_min += 1;
        }
        a_max += 1;
    }
    return {min: a_min, max: a_max};
}

function add_module(name, fn) {
    accept[name] = fn;
}

function accept_eval_type(type, code, t) {
    return accept[type](code, t);
}

// Переместить в types
function get_type(code) {
    if (code.type == 'var' && code.value != null) {
        return get_type(code.value);
    }
    
    // Тип может быть и обьектом
    if (code.type == 'class') {
        return scope.get_global(code.name) || code;
    }
    return code;
}

exports.add_module = add_module;
exports.accept_eval = accept_eval;
exports.accept_eval_type = accept_eval_type;
exports.accept_stmt = accept_stmt;
exports.is_node = is_node;
exports.is_name_ns = is_name_ns;
exports.ast_type = ast_type;
exports.args_count = args_count;
exports.get_type = get_type;
exports.stat = stat;

exports.plugin = plugin;

exports.isUpperCamelCase = isUpperCamelCase;
exports.isLowerCamelCase = isLowerCamelCase;

