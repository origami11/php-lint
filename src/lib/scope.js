﻿"use strict";

class Stack {
    constructor() {
        this.data = [];
    }

    push(value) {
        if (value == null) {
            throw new Error('Set unknown value');
        }
        this.data.push(value);
    }

    pop() {
        return this.data.pop();
    }
}

class ClassScope {
    constructor() {
        // В PHP по умолчанию локальное окружение
        this.data = null;
    }
    
    begin(t_var) {
        this.data = t_var;
    }

    set_method(name, type) {
        this.data.methods[name] = type;
    }

    set_prop(name, type) {
        this.data.property[name] = type;
    }

    set_const(name, type) {
        this.data.constants[name] = type;
    }

    end() {
        this.data = null;
    }

    get_method(name, type) {
        return this.data.methods[name];
    }
}

class LocalScope {
    constructor() {
        // В PHP по умолчанию локальное окружение
        this.hash = {};
        this.uses = {};
        this.fn = null;
    }
}

class Scope {
    constructor() {
        this.stack = [];
        this.globals = {};
        this.file = '';

        this.superglobals = {};
        this.ns = {name: null, uses: {}};

        this.nstack = new Stack();
        this.fstack = new Stack();
    }

    suggest(name) { 
        var result = [];
        var list = name.split('\\');
        var part = list[list.length - 1];
        
        for(var i in this.globals) {
            if (this.globals.hasOwnProperty(i)) {
                var item = this.globals[i];
                for(var k in item) {
                    if (item.hasOwnProperty(k) && item[k].type == 'class' && k.indexOf(part) >= 0) {
                        result.push(k);
                    }
                }                
            }
        }
        return result;
    }

    add_uses(name, alias) {
        var pos = name.lastIndexOf('\\');
        var key = name, classPath = name;
        if (pos >= 0) {
            key = name.substr(pos + 1);
        }

        if (alias) {
            key = alias;
        }

        if (this.ns.uses.hasOwnProperty(key)) {
            return false;
        }

        this.ns.uses[key] = classPath;
        return true;
    }

    begin_ns(name) {
        this.nstack.push(this.ns);
        this.ns = {name: name, uses: {}};
    }

    end_ns() {
        this.ns = this.nstack.pop();
    }

    begin_file(name) {
        var lower = name.toLowerCase();
        this.begin_ns();

        this.fstack.push(this.file);

        this.file = lower;
        if (!this.globals.hasOwnProperty(this.file)) {
            this.globals[this.file] = {};
        }
        if (!this.superglobals.hasOwnProperty(this.file)) {
            this.superglobals[this.file] = {};
        }
    }

    end_file() {
        this.end_ns();
        this.file = this.fstack.pop();
    }

    clear_file(filename) {
        if (this.globals.hasOwnProperty(filename)) {
            this.globals[filename] = {};
        }

        if (this.superglobals.hasOwnProperty(filename)) {
            this.superglobals[filename] = {};
        }
    }

    begin() {
        this.stack.push(new LocalScope());
        // Локальное окружение
    }

    end() {
        return this.stack.pop();
    }

    top() {
        return this.stack[this.stack.length - 1];
    }

    set(name, value, optional) {
        if (this.stack.length > 0) {
            var top = this.top();
            if (!top.uses.hasOwnProperty(name)) {
                top.uses[name] = optional ? 1 : 0;
            }
            top.hash[name] = value;
        } else {
            var scope = this.globals[this.file];
            if (scope.hasOwnProperty(name) && value.type == 'function') {
                value.override = scope[name];
            }

            scope[name] = value;
        }
    }

    set_global(name, value) {
        if (this.get_global(name)) {
            console.log('var already defined ' + name);
        }
        this.globals[this.file][name] = value;
    }

    set_global_class(name, value) {
        var altname = this.ns.name ? this.ns.name + '\\' + name : name;        
        this.globals[this.file][altname] = value;
    }

    set_super(name, value) {
        this.superglobals[this.file][name] = value;
    }

    get(name) {
        if (this.stack.length > 0) {
            var top = this.top();
            if (top.hash.hasOwnProperty(name)) {
                top.uses[name]++;
                return top.hash[name];
            }

            if (top.fn && top.fn.use.indexOf(name) >= 0) {
                var last = this.stack.pop();
                var result = this.get(name);
                this.stack.push(last);
                return result;
            }

            return this.get_superglobal(name);
        } 

        return this.get_global(name);
    }

    get_superglobal(name) {
        for(var i in this.superglobals) {
            if (this.superglobals.hasOwnProperty(i)) {
                var item = this.superglobals[i]; 
                if (item.hasOwnProperty(name)) {
                    return item[name];
                }               
            }
        }

        return false;
    }

    get_global(name) {
        for(var i in this.globals) {
            if (this.globals.hasOwnProperty(i)) {
                var item = this.globals[i];                
                if (item.hasOwnProperty(name)) {
                    return item[name];
                }
            }
        }

        return this.get_superglobal(name);
    }

    resolveNsName(name, resolution) {
        if (resolution == 'fqn' && name.charAt(0) == '\\') {
            return name.substr(1);
        }

        if (resolution == 'uqn') {
            if (this.ns.uses.hasOwnProperty(name)) {
                return this.ns.uses[name];
            }

            return this.ns.name ? this.ns.name + '\\' + name : name;
        }

        if (resolution == 'qn' && this.ns.name) {
            return this.ns.name + '\\' + name;
        }
        return name;
    }

    get_global_ns(path) {
        var name = path;
        return this.get_global(name);
    }
}

var class_scope = new ClassScope();
var stack = new Stack();
var scope = new Scope();

exports.Scope = Scope;
exports.LocalScope = LocalScope;
exports.ClassScope = ClassScope;
exports.Stack = Stack;

exports.scope = scope;
exports.stack = stack;
exports.class_scope = class_scope;

