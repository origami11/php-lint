﻿//
const { options } = require('./options');
const util = require('util');
const cli = require('cli-color');
const { scope } = require('./scope');

var message_count = 0;

var error_type = {
    type: 'Type "%s" is incompatible with "%s"',
    noprop: 'Unknown property "%s" of class "%s"',
    ast: 'Unknown ast for "%s"',
    block: 'Use block expression',
    novar: 'Cannot find variable "%s"',
    noid: 'Unknown identifier "%s"',
    usedup: 'Duplicate uses name',
    noconst: 'Cannot find constant "%s"',
    noargs: 'Wrong number of arguments for "%s"',
    override: 'Declaration of "%s" should be compatible "%s"',
    noclass: 'Unknown class "%s"',
    noclassalt: 'Unknown class "%s", but find "%s"',
    notstatic: 'Method "%s" called as static',
    noimethod: 'Method "%s" not implemented',
    classuse: 'Class "%s" has same name with one of uses',
    nonsclass: 'Unknown class "%s" in namespace %s',
    nofile: 'File not found "%s"',
    noclassfile: 'File not found "%s"',
    noname: 'Cannot find name "%s"',
    nothis: 'Wrong $this context',
    nosys: 'Unknown sys "%s"',
    classname: 'Bad class name "%s"',
    fnname: 'Bad function name "%s"',
    noret: 'Return outside of function',
    unused: 'Unused variable "%s"',
    badconstr: 'Use __construct instead of %s',
    noparse: '%s',
    no_comment_type: 'Bad comment type "%s"'
};

var _messages = new Map();
function message(pos, type, args) {
    args = args || [];
    if (is_visible(type)) {
        push_message(scope.file, pos, type, args);
    }
}

function format_message(file, m) {
    var args = m.args;
    var loc = m.loc;
  
    return cli.red(file + (loc && loc.start ? "(" + loc.start.line + "," + loc.start.column + ")" : "") + ":") + '(' + m.type + ') ' + util.format.apply(null, [error_type[m.type]].concat(m.args)) + ".";
}

function is_visible(type) {
    var list = options.message.split(',');
    var flag = false;
    for(var i = 0; i < list.length; i++) {
        if (list[i] == 'all' || list[i] == type) flag = true;
        if (list[i].charAt(0) == '-' && list[i].substr(1) == type) flag = false;
    }
    return flag;
}

function clear_messages() {
    _messages = new Map();
}

function get_count() {
    return _messages.length;
}

function push_message(source_file, pos, type, args) {
    if (!_messages.has(source_file)) {
        _messages.set(source_file, []);
    }

    var arr = _messages.get(source_file);
    arr.push({loc: pos, type: type, args: args});
}

function printMessages(mx) {
    var c = 0;
    mx.forEach((list, f) => {
        list.forEach(m => {                
            if(is_visible(m.type)) {
                c++;
                console.log(format_message(f, m));
            }
        })
    })
    console.log('PHP Errors:', c);
}

function get_messages() {
    return _messages; 
}

exports.formatMessage = format_message;
exports.pushMessage = push_message;
exports.clearMessages = clear_messages;
exports.printMessages = printMessages;
exports.isVisible = is_visible;
exports.getMessages = get_messages;
exports.error_type = error_type;

exports.message = message;
