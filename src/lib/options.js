
exports.options = {
    cache: false,
    path: './',    
    cwd: './',    
    tspath: './',
    include: [],
    scope: false,
    trace: false,
    resolve: false,
    path_filter: null,
    runtime: true,
    strict: true,
    debug: false,
    nsmap: {},
    message: 'all,-block,-classname,-fnname'    
};
