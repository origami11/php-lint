﻿
const { accept_eval } = require('./ast');
const { class_scope, scope, stack } = require('./scope');
const { unify_types } = require('./unify');
const { clearMessages, pushMessage, message } = require('./messages');
const { options } = require('./options');

const phpParser = require('php-parser');
const path = require('path');

const { t_var, t_string, t_number, t_array, t_any} = require('./types');

const PEG = require("pegjs");

const fs = require('fs');

var loaded = {};

var parser = PEG.buildParser(fs.readFileSync(path.join(__dirname, '/../peg/types.peg'), 'utf-8'), {allowedStartRules: ['Program', 'TypeDecl', 'TypeOption']});

var reader = new phpParser({
    parser: {
        extractDoc: false,
        suppressErrors: false,
        php7: true
    },
    ast: {
        withPositions: true,
        withSource: false
    },
    lexer: {
        short_tags: false,
        asp_tags: false
    }
});

function loadDefs(filename, mode) {
    if (isLoaded(filename)) {
        return;
    }

    loaded[filename] = mode;

    var data = fs.readFileSync(filename, 'utf-8');
    var ast = parser.parse(data);

    var refs = findRefs(data);
    var basePath = path.dirname(filename);
    var localRefs = refs.map(item => path.join(basePath, item));

    localRefs.forEach(function (def) {
        loadDefs(def, 1);
    });

    scope.begin_file(filename);
    ast.forEach(function (item) {
        if (item.role == 'const') {
            scope.set_super(item.name, item.type);
        } else if (item.role == 'super') {
            scope.set_super(item.name.substr(1), item.type);
        } else if (item.role == 'var') {
            scope.set(item.name.substr(1), item.type);
        } else {
            scope.set(item.name, item.type);
        }
    });
    scope.end_file();
}

function findRefs(code) {
    var pattern = /\/\/\/\s*<reference\s+path="([^"]*)"\s*\/>/gi; //"
    var match, result = [];
    while (match = pattern.exec(code)) {
        result.push(match[1]);
    }
    return result;
}

function isLoaded(filename) {
    return loaded.hasOwnProperty(filename) && loaded[filename] > 0;
}

function loadFromAst(astfn, filename) {
    var ast = astfn();
    if (ast) {
        var tmp_class = class_scope.data;
        class_scope.data = null;

        var basePath = path.dirname(filename);
        var localRefs = ast.refs.map(item => path.join(basePath, item));

        localRefs.forEach(function (def) {
            if (def.substr(-3) == 'php') {
                loadFromFile(def);
            } else {
                loadDefs(def, 1);
            }
        });
    
        scope.begin_file(filename);
    
        accept_eval(ast);
        class_scope.data = tmp_class;
    
        scope.end_file();
    }        

    if (options.resolve && scope.file) {
        console.log('return to file ', scope.file);
    }

    return ast;
}


function timeStat(action, filename, cc) {
    var begin = Date.now();
    cc();
    console.log(action, filename, (Date.now() - begin) / 1000 + 's');
}

function loadFromString(code, filename) {
    return loadFromAst(function () {
        var ast;
        try {
            ast = reader.parseCode(code, filename);
        } catch(e) { 
            if (e.message.indexOf('Parse Error') >= 0) {
                var pos = {start: {line: e.lineNumber, column: e.columnNumber}};
                pushMessage(e.fileName.toLowerCase(), pos, 'noparse', [e.message]);
            } else {
                console.log(e.stack);
            }
            return null;
        }

        var refs = findRefs(code);
        ast.refs = refs;

        return ast;
    }, filename);
}

function loadFromFile(filename) {
    var lower = filename.toLowerCase();
    if (isLoaded(lower)) {
        return;
    } 

    loaded[lower] = 1;

    var astfile;
    if (options.resolve) {
        console.log('load from file ', filename);
    }        

    var code = fs.readFileSync(filename, 'utf-8');
    var ast = loadFromString(code, filename);
}

function loadText(code, filename) {
    loadFromString(code, filename);
}

function loadFile(filename) {
    var fullname = path.resolve(filename);
    loadFromFile(fullname);
    return fullname;
}

function evalNsPath(path, parts) {
    var pathParts = path.split('/');
    var result = pathParts.map(x => {
        if (x.charAt(0) == '$') {
            var index = parseInt(x.charAt(2), 10);
            var mod = x.charAt(1);
            if (mod == 'c') {
                return parts[index].toLowerCase();
            }
            if (mod == 's') {
                return parts.slice(index).join('/');
            }            
        }
        return x;
    });   
    return result.join('/');
}

function resolveNamespace(name) {
    var parts = name.split('\\');
    var nsmap = options.nsmap;
    var key = parts[0];        
    if (nsmap.hasOwnProperty(key)) {
        return evalNsPath(nsmap[key], parts);
    }
    return null;
}

function apply_filter(filters, n, file) {
    if (filters && n < filters.length) {
        if (filters[n] == 'lower') {
            return file.toLowerCase();
        }
    }
    return file;
}

function resolveClassPath(name, loc) {
    var nsfile = resolveNamespace(name);
    if (nsfile) {
        var filenamens = path.join(options.cwd, nsfile + ".php");
        if (fs.existsSync(filenamens)) {
            return path.resolve(filenamens)
        }
    }

    var file = name.split('_').join(path.sep);
    var list = options.path.split(';');

    var filename = list.map((item, n) => {
        return path.join(options.cwd, item, apply_filter(options.path_filter, n, file) + ".php")
    });
    filename.push(path.join(options.tspath, file.toLowerCase() + ".d.ts"));

    for(var i = 0; i < filename.length; i++) {
        if (fs.existsSync(filename[i])) {
            return path.resolve(filename[i]);
        }
    }

    message(loc, 'nofile', [file]);
    return null;
}

function resolveFilePath(name, loc) {
    // localFile
    var list = options.path.split(';');

    var filename = list.map(item => path.join(item, name));
    filename.push(path.join(path.dirname(scope.file), name));

    var parts = path.parse(name);        
    filename.push(path.join(options.tspath, parts.dir, parts.name + '.d.ts'));
            
    for(var i = 0; i < filename.length; i++) {
        if (fs.existsSync(filename[i])) {
            return path.resolve(filename[i]);
        }
    }
    message(loc, 'nofile', [name]);
    return null;
}

function getPreloadedClass(name, loc) {
    var tobj = scope.get_global_ns(name);
    if (!tobj) {
        var pathName = resolveClassPath(name, loc);
        if (pathName) {
            if (options.resolve) {
                console.log('resolved', name, 'as', pathName);
            }
            if (path.extname(pathName) == '.php') {
                loadFromFile(pathName);
            } else {
                loadDefs(pathName, 1);
            }
            tobj = scope.get_global_ns(name);
        }
    }
    return tobj;
}

function getNsResolution(name) {
    if (name.charAt(0) == '\\') return 'fqn';
    if (name.indexOf('\\') > 0) return 'qn';
    return 'uqn';
}

function getTypeOption(target) {
    if (target._comment) {
        var typeText = target._comment.substr(2, -2);
        if (typeText.charAt(0) == '<') {
            return parser.parse(typeText, {startRule: 'TypeOption'});
        }

        throw new Error('Unknown type option "' + typeText + '"')
    }
    return null;
}

function getCTypeName(target) {
   
}

function getCommentType(target, loc) {
    if (target._comment) {
        var text = target._comment;
        var typeText = text.substring(2, text.length - 2);
        if (typeText.charAt(0) == ':') {
            try {
                var r = parser.parse(typeText, {startRule: 'TypeDecl'});
                if (r.type == 'class') {
                    var name = scope.resolveNsName(r.name, getNsResolution(r.name));
                    var varType = getPreloadedClass(name, loc);

                    if (!varType) {
                        message(loc, 'noclass', [name]);
                    }
                    return varType;
                }
                return r;
            } catch(e) {
                message(loc, 'no_comment_type', [typeText]);
            }
        } else {
            message(loc, 'no_comment_type', [typeText]);
        }
    }
    return null;

    /*if (typeName == 'any') {
        varType = t_any;
    } else if (typeName == 'number') {
        varType = t_number;
    } else if (typeName == 'array') {
        varType = t_array(t_any, t_any);
    } else if (typeName == 'string') {
        varType = t_string();
    } else if (typeName) {
        var name = scope.resolveNsName(typeName, getNsResolution(typeName));
        varType = getPreloadedClass(name, loc);

        if (!varType) {
            message(loc, 'noclass', [name]);
        }
    }
    return varType;
    */
}

function getArgumentType(arg) {
    var type_id = arg.type, type = t_var();
               
    if (type_id && type_id.kind == 'identifier') {
        type_name = type_id.name;

        if (type_name == 'String') {
            type = t_string();
        } else if (type_name == '\\array') {
            type = t_array();
        } else if (type_name != 'mixed') {
            var name = scope.resolveNsName(type_name, type_id.resolution)
            type = getPreloadedClass(name, arg.loc);
            if (!type) {
                type = t_var();
                message(arg.loc, 'noclass', [type_name]);
            }
        }
    }

    var varType = getCommentType(arg, arg.loc);
    if (varType) {
        type = varType;
    }

    if (arg.value) {
        accept_eval(arg.value);
        type = stack.pop();
    }

    return type;
}


function reset() {
    clearMessages();
    for(var i in loaded) {
        if (loaded.hasOwnProperty(i) && loaded[i] == 1) {
            scope.clear_file(i);
            loaded[i] = 0;
        }
    }

    options.include.forEach(item => {
        loadDefs(path.join(options.tspath, item), 1);
    });
}

exports.getCommentType = getCommentType;
exports.getArgumentType = getArgumentType;

exports.loadDefs = loadDefs;
exports.loadFile = loadFile;
exports.loadFromFile = loadFromFile;
exports.loadText = loadText;
exports.reset = reset;
exports.getPreloadedClass = getPreloadedClass;
exports.resolveFilePath = resolveFilePath;
