﻿//

const { scope } = require('./scope');
const { message } = require('./messages');
const { getType } = require('./print');

function unify_list(list, b) {
    for(var i = 0; i < list.length; i++) {
        if (unify_types(list[i], b)) {
            return true;
        }
    }
    return false;
}

function unify_sets(a, b) {
    if (a.length == b.length) {
        for(var i = 0; i < a.length; i++) {
            if (!unify_types(a[i], b[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function get_ntype(a) {
    if (a.type === 'var') {
        if (a.value !== null) {
            return get_ntype(a.value);
        }
        return a;
    }
    return scope.get_global(a.type) || a;
}

function is_subclass(x, y) {
    if (x.name == y.name) {
        return true;
    } else if (x.valid) {
        var parent = x.parent;
        if (parent.length > 0) {
            var c = scope.get_global_ns(parent);
            return is_subclass(c, y);    
        }
    }
    return false;
}

function is_implements(x, y) {
    if (!x.valid) {
        x = scope.get_global_ns(x.name);
        if (!x) return false;
    }
//    if (!x.implement) {
//        console.log(x);
//    }
    if (x.implement.indexOf(y.name) >= 0) {
        return true;
    } else {
        var parent = x.parent;
        if (parent.length > 0) {
            var c = scope.get_global_ns(parent);
            return is_implements(c, y);
        }
    }
    return false;
}

function is_string_compat(b) {
    if (b.type === 'string' || b.type === 'number') {
        return true;
    }    
    if (b.type === 'case') {
        for(var i = 0; i < b.list.length; i++) {
            if (!is_string_compat(b.list[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function is_string_valid(b) {
    return (b.type === 'string' && b.subtype === 'valid') || b.type === 'number' || b.type === 'boolean';
}

/**
 * Унификация не ассоциативная т.е 
        unify_types(ta, tb) != unify_types(tb, ta)
 * ta Пример типа с которым сравнивают
 * tb Типа который сравнивают
 */
function unify_types(ta, tb) {

    if (!ta || !tb) {
        throw new Error('Unknown types ' + ta + ' ' + tb);
    }

    var a = get_ntype(ta);
    var b = get_ntype(tb);

//    console.log(a.type, b.type);

    if (a === b) {
        return true;
    }

    if (a.type === 'var') {
        a.value = b;
        return true;
    }

    if (b.type === 'var') {
        b.value = a;
        return true;
    }        

    if (a.type === 'array' && b.type === 'array') {        
        return unify_types(a.key, b.key) && unify_types(a.value, b.value);
    }    

//    if (a.type == 'string' && b.type == 'string') {
//        return true;
        /*if (a.subtype == 'any') {
            return true;
        }            
        return a.subtype == b.subtype;*/
//    }

    /*if (a.type == 'string' && a.subtype == 'valid') {
        return is_string_valid(b);
    }*/

    if (a.type === 'string' && is_string_compat(b)) {
        return true;
    }

    if (a.type === 'any' || b.type === 'any')  {
        return true;
    }    
    
    if (a.type === 'case') {
        if (b.type === 'case') {
            return unify_sets(a.list, b.list);
        }
        return unify_list(a.list, b);
    }

    if (a.type === 'interface' && b.type === 'class') {
        if (is_implements(b, a)) {
            return true;
        }
        return false;
    }

    if (a.type === 'class' && b.type === 'interface') {
        if (!a.name && a.implement.length == 0) {
            a.implement.push(b.name);
            return true;
        }
        if (is_implements(a, b)) {
            return true;
        }
        return false;
    }

    if (a.type === 'interface' && b.type === 'interface') {
        if (!a.name) {
            a.name = b.name;
            return true;
        }
        if (!b.name) {
            b.name = a.name;
            return true;
        }
        return false;
    }

    if (a.type === 'class' && b.type === 'class') {
        if (!a.name) {
            a.name = b.name;
            return true;
        }
        if (!b.name) {
            b.name = a.name;
            return true;
        }
        if (a.name === 'stdClass') {
            return true;
        }
        if (is_subclass(b, a)/* || is_subclass(b, a)*/) {
            return true;
        }
        return false;
    }

    if (a.type === 'Function' && (b.type === 'Function' || b.type == 'function')) {
        return true;
    } 

    if (b.type === 'Function' && (a.type === 'Function' || a.type == 'function')) {
        return true;
    }

    if (a.type === 'function' && b.type === 'function') {
        if (a.args.length == b.args.length) {
            for(var i = 0; i < a.args.length; i++) {
                if (!unify_types(a.args[i].type, b.args[i].type)) {
                    return false;
                }
            }
        }
        return unify_types(a.ret, b.ret);
    }

    if (a.type === b.type) {
        return true;
    }

    return false;
}

function unify(pos, a, b) {
    if (!unify_types(a, b)) {
        message(pos, 'type', [getType(a, false, 0), getType(b, false, 0)]);
    }
}

function set_variable(name, type) {
    var v = scope.get(name);
    if (v) {
        unify(v.position, v, type);
    } else {
        scope.set(name, type);
    }
}

exports.unify_types = unify_types;
exports.unify = unify;
exports.set_variable = set_variable;

