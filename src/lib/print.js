﻿const util = require('util');
const { scope } = require('./scope');

var maxLevel = 5;

function getType(a, detail, level) {
    level = level || 0;

    if (level > maxLevel) {
        return "...";
    }

    if (!a) {
        return 'unknown';
    }
    if (a.type == 'array') {
        return util.format('array(%s)<%s>', getType(a.key, false, level + 1), getType(a.value, false, level + 1));
    }
    if (a.type == 'number') {
        return 'number';
    }
    if (a.type == 'resource') {
        return 'resource';
    }
    if (a.type == 'class') {
        if (detail && a.valid) {
            var m_list = [], implement = "";
            for(var i in a.constants) {
                if (a.constants.hasOwnProperty(i)) {
                    m_list.push('        const ' + i + ": " + getType(a.constants[i], false, level + 1) + ';\n');
                }
            } 
            for(var i in a.property) {
                if (a.property.hasOwnProperty(i)) {
                    m_list.push('        ' + i + ": " + getType(a.property[i], false, level + 1) + ';\n');                
                }
            } 
            for(var i in a.methods) {
                    if (a.methods.hasOwnProperty(i)) {
                    m_list.push('        ' + i + ": " + getType(a.methods[i], false, level + 1) + ';\n');
                }
            } 

            if (a.implement.length > 0) {
                implement = " implement " + a.implement.join(",");
            }
            
            return 'class ' + a.name + implement + ' {\n' + m_list.join('') + '    }';
        } else  {
            return 'class ' + a.name + ((a.valid) ? '' : '*');
        }
    }
    if (a.type == 'string') {
        return 'string' + ( a.subtype == 'any' ? '' : '<' + a.subtype + '>');
    }
    if (a.type == 'function') {
        if (true) {
            return util.format('function (%s): %s', a.args.map(function (arg) {
                return arg.name + (arg.optional ? "?" : '') + ': ' + getType(arg.type, false, level + 1);
            }).join(", "), getType(a.ret, false, level + 1));
        } else  {
            return 'function ' + a.name;
        }
    }
    if (a.type == 'boolean') {
        return 'boolean';
    }
    if (a.type == 'Function') {
        return 'Function';
    }
    if (a.type == 'case') {
        return a.list.map((x) => getType(x, detail, level + 1)).join(" | ");
    }
    if (a.type == 'any') {
        return 'any';
    }
    if (a.type == 'void') {
        return 'void';
    }
    if (a.type == 'var') {
        return getType(a.value, false, level);
    }
    if (a.type) {
        return getType(scope.get_global(a.type), detail, level + 1);    
    }
}

exports.getType = getType;
