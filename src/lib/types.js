﻿
const { message } = require('./messages');
const { scope } = require('./scope');

function t_factory(name) {
    return {'type': name};
}

var t_number = t_factory('number');
var t_any = t_factory('any');
var t_void = t_factory('void');

var t_boolean = t_factory('boolean');
var t_resource = t_factory('resource');
var t_Function = t_factory('Function');

function t_string(subtype, value) {
    return {type: 'string', subtype: subtype || 'any'};
}

function t_case(list) {
    return {type: 'case', list: list};
}

function t_var() {
    return {type: 'var', value: null};
}

function t_func(name, ret, args) {
    return {type: 'function', ret: ret || t_var(), args: args || [], name: name, use: [], hasret: false, is_static: false};
}

function t_array(key, val) {
    return {type: 'array', key: key || t_var(), value: val || t_var()};
}

function t_hash() {
    return {type: 'hash', key: {}};
}

function has_interface(obj, name) {
    if (obj && obj.type == 'class' && obj.implement) {
        return (obj.implement.indexOf(name) >= 0) || has_interface(obj.parent, name);
    }
    return false;
}

function is_extends(obj, name) {
    if (obj && obj.type == 'class' && obj.parent) {        
        return (obj.parent.indexOf(name) >= 0);
    }
    return false;
}

function parent_props(parent, name) {    
    var obj = scope.get_global_ns(parent);
    if (obj) {
        return get_class_prop(obj, name);
    }
    return false;
}

function get_class_prop(obj, name) {
    if (obj.name == 'stdClass') {
        return t_any;
    }

    if (!obj.valid) {
        return false;
    }

    if (obj.methods.hasOwnProperty(name)) {
        return obj.methods[name];
    }

    if (obj.property.hasOwnProperty(name)) {
        return obj.property[name];
    }

    if (obj.constants.hasOwnProperty(name)) {
        return obj.constants[name];
    }

    if (obj.parent.length > 0) {
        return parent_props(obj.parent, name);
    }
    return false;
}

function parent_interface_props(parent, name) {
    var obj = scope.get_global_ns(parent);
    if (obj) {
        return get_interface_prop(obj, name);
    }
    return false;
}

function get_interface_prop(obj, name) {
    if (!obj.valid) {
        return false;
    }

    if (obj.methods.hasOwnProperty(name)) {
        return obj.methods[name];
    }
 
    if (obj.constants.hasOwnProperty(name)) {
        return obj.constants[name];
    }

    if (obj.parent.length > 0) {
        return parent_interface_props(obj.parent, name);
    }
    return false;
}

function class_prop(pos, obj, name) {
    var prop = get_class_prop(obj, name);
    if (!prop) {
        message(pos, 'noprop', [name, obj.name]);
    }
    return prop;
}

function t_class(name, valid) {
    return {
        type: 'class', 
        name: name, 
        parent: [], 
        implement: [], 
        methods: {}, 
        property: {}, 
        constants: {}, 
        typevar: [],
        valid: valid
    };
}

function t_interface(name, valid) {
    return {
        type: 'interface', 
        name: name, 
        parent: [], 
        implement: [], 
        methods: {}, 
        constants: {}, 
        property: {}, 
        valid: valid
    };
}

function compare_type(a, b) {
    return a.type == b.type;
}

// Перед должен вызываться get_type чтобы получить унифицированный вид
function is_type(code, type) {
//    if (code.type == 'var' && code.value != null) {
//        return is_type(code.value, type);
//    }
    return (code.type == type);
}

exports.t_number = t_number;
exports.t_string = t_string;
exports.t_any = t_any;
exports.t_void = t_void;
exports.t_array = t_array;
exports.t_boolean = t_boolean;
exports.t_factory = t_factory;
exports.t_class = t_class;
exports.t_interface = t_interface;
exports.t_case = t_case;
exports.t_var = t_var;
exports.t_func = t_func;

exports.is_type = is_type;

exports.get_class_prop = get_class_prop;
exports.get_interface_prop = get_interface_prop;
exports.class_prop = class_prop;
exports.parent_props = parent_props;
exports.has_interface = has_interface;
exports.is_extends = is_extends;
