﻿const { plugin } = require('../lib/ast');
const { message, error_type } = require('../lib/messages');

error_type['linkhttp'] = 'protocol should be "https"';

plugin.before('string', function (code) {
    var s = code.value;
    if (s.match(/^http:\/\//)) {
        message(code.loc, 'linkhttp', []);
    }
});