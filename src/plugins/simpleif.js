﻿const { plugin } = require('../lib/ast');
const { message, error_type } = require('../lib/messages');

error_type['simpleif'] = '([expr] ? true: false) can be simplified to [expr]';

plugin.before('retif', function (code) {
    var _true = code.trueExpr;
    var _false = code.falseExpr;
    if (_true && _false && _true.kind == 'boolean' && _false.kind == 'boolean' && _true.value == true && _false.value == false) {
        message(code.loc, 'simpleif', []);
    }
});