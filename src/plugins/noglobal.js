﻿const { plugin } = require('../lib/ast');
const { message, error_type } = require('../lib/messages');

error_type['noglobal'] = 'don\'t use global variables';

plugin.before('global', function (code) {
    message(code.loc, 'noglobal', []);
});