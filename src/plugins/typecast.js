﻿const { plugin } = require('../lib/ast');
const { message, error_type } = require('../lib/messages');

error_type['typecast'] = 'intval([expr]) can be optimized to (int)[expr]';

plugin.before('call', function (code) {
    if (code.what.kind == 'identifier' && code.what.name == 'intval' && code.arguments.length == 1) {
        message(code.loc, 'typecast', []);
    }    
});