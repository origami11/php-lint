﻿const { plugin } = require('../lib/ast');
const { message, error_type } = require('../lib/messages');

error_type['forcount'] = 'count([expr]) can be moved outside loop';

plugin.before('for', function (code) {
    if (code.test.length = 1 && code.test[0].kind == 'bin') {
        var r = code.test[0].right;
        if (r.kind == 'call' && r.what.name == 'count') {
            message(code.loc, 'forcount', []);
        }
    }    
});