
var NEWLINE = {"type": "newline"};
var LEFT = {"type": "left"};
var RIGHT = {"type": "right"};
var DNEWLINE = {"type": "double"};

function repeat(length) {
    return Array(length + 1).join(" ");
}

function join(sp, arr) {
    var result = [];
    if (arr.length > 0) {
        for(var i = 0; i < arr.length - 1; i++) {
            result.push(arr[i], sp);
        }
        result.push(arr[arr.length - 1]);
    }
    return result;
}

// Вывод строки
var left = 0;
var colors = [];
var newline = false;
function p_print(s, message) {
    if (typeof message == 'undefined') {
		//throw new Error('unknown print');
        console.log('unknown print');
    } else if (message.constructor === Array) {
        for(var i = 0; i < message.length; i++) {
            p_print(s, message[i]);
        }
    } else if (message.constructor === String || message.constructor === Number) {
        if (newline) {
            s.push(repeat(left));
            newline = false;
        }
        s.push(message);
    } else if (message.type === "list") {
        // Вывод подстроки
        p_print(s, message.data);
    } else if (message.type === "left") {
        // Вывод со смещением
        left += 4;
    } else if (message.type === "right") {
        left -= 4;
    } else if (message.type === "newline") {
        if (!newline) {
            newline = true;  
            s.push("\n");
        } 
    } else if (message.type === "double") {
        if (!newline) {
            newline = true;  
            s.push("\n\n");
        }
    } 
}

function print(message) {
    var s = [];
    left = 0;
    p_print(s, message);
    return s.join("");
}

exports.LEFT = LEFT;
exports.RIGHT = RIGHT;
exports.NEWLINE = NEWLINE;
exports.DNEWLINE = DNEWLINE;
exports.print = print;
exports.join = join;

exports.NLEFT = [LEFT, NEWLINE];
exports.NRIGHT = [RIGHT, NEWLINE];
