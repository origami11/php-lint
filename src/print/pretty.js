﻿var pp = require('./print');

function ast_type(code) {
    return code.type;
}

function message(pos, type, str) {
    console.log(str);
}

function accept_eval(code) {    
    if (code && accept.hasOwnProperty(ast_type(code))) {
        return accept[ast_type(code)](code);
    }
    message(null, 'ast', code.type);
    return [];
}

function accept_block(body) {
    if (Array.isArray(body)) {
        return ["{", pp.NLEFT, body.map(accept_stmt), pp.NRIGHT, "}"];
    } else {
        return accept_stmt(body);
    }
}

function accept_stmt(code) {
    if (code) {
        if (["if", "for", "while"].indexOf(code.type) >= 0) {
            return accept_eval(code);
        } else {
            return [accept_eval(code), ";", pp.NEWLINE];
        }
    }
    return -1;
}

var accept = {
    'program': function(code) {
        var stmt_list = code.stmt;
        var list = stmt_list.map(accept_stmt);

        return ["<?php", pp.DNEWLINE, list];
    },
    'string': function(code) {
        return ["\"", code.value.replace('\n', "\\n"), "\""];
    },        
    'number': function(code) {
        return code.value;
    },        
    'var': function(code) {
        return code.name;
    },        
    'const': function(code) {     
        return code.value;
    },        
    'ns': function(code) {
        return code.args.join("\\");
    },        
    'set': function(code) {
        return [accept_eval(code.target), " = ", accept_eval(code.expr)];
    },        
    'offset': function(code) {
        return [accept_eval(code.target), "[", accept_eval(code.value), "]"];        
    },        
    'call': function(code) {
        return [accept_eval(code.fn), "(", pp.join(", ", code.args.map(accept_eval)), ")"];
    },        
    'sys': function (code) {
        var fn = code.fn;
        var args = code.args;

        if (Array.isArray(args)) {
            return [fn, " ", pp.join(", ", args.map(accept_eval))];            
        }

        return [fn, " ", accept_eval(args)];
    },
    'bool': function (code) {
        var opname = {
            '~': "==",            
            '&': '&&',
        };

        return [accept_eval(code.arg1), " ", (opname.hasOwnProperty(code.op) ? opname[code.op] : code.op), " ", accept_eval(code.arg2)];
    },
    'bin': function (code) {
        return [accept_eval(code.arg1), " ", code.op, " ", accept_eval(code.arg2)];
    },
    'unary': function (code) {
        return [code.op, accept_eval(code.arg1)];
    },
    'if': function (code) {    
        return ["if", " (", accept_eval(code.test), ") ", accept_block(code.b_then), code.b_else ? [" else ", accept_block(code.b_else)] : [], pp.DNEWLINE];
    },
    'prop': function (prop) {
        return [accept_eval(prop.obj), "->", prop.value.value]
    },
    'array': function (prop) {
        return ["array(", pp.join(", ", prop.values.map((x) => x.key ? [accept_eval(x.key), " => ", accept_eval(x.value)] : accept_eval(x.value) )), ")"]
    },
    'new': function (code) {
        return ["new ", code.name.join("\\"), "(", pp.join(", ", code.args.map(accept_eval)), ")"];
    },
    'function': function (code) {
        var use = [];
        if (code.use.length > 0) {
            use = ["use(", pp.join(", ", code.use.map(x => x[1])), ") "];
        }

        return ['function ', "(", pp.join(", ", code.args.map(x => x.name)), ") ", use, accept_block(code.body)];
    },
    'return': function (code) {
        return ["return ", accept_eval(code.value)];
    },
    'post': function (code) {
        var opname = {
            '+': "++",            
            '-': '--',
        };
        return [accept_eval(code.arg1), opname[code.op]];
    },
    'for': function (code) {
        return ["for (", code.init.map(accept_eval), "; ", code.test.map(accept_eval), "; ", code.next.map(accept_eval), ") ", accept_block(code.body)];
    }
};

exports.print_php = function (ast) {
    return pp.print(accept_eval(ast));
}  
