﻿const PGSQL_ASSOC: number;

function pg_escape_string($str: string): string;
function pg_fetch_array($res: any, $row?: number,  $result_type?: number): array<any>;
function pg_query($res: any, $query: string): any;