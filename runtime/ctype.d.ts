
function ctype_alnum($text: string): boolean;
function ctype_alpha($text: string): boolean;
function ctype_cntrl($text: string): boolean;
function ctype_digit($text: string): boolean;
function ctype_graph($text: string): boolean;
function ctype_lower($text: string): boolean;
function ctype_print($text: string): boolean;
function ctype_punct($text: string): boolean;
function ctype_space($text: string): boolean;
function ctype_upper($text: string): boolean;
function ctype_xdigit($text: string): boolean;