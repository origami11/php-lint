
super $GLOBALS: array<any>;
super $_SERVER: array<string>;
super $_GET: array<string>;
super $_POST: array<string>; 
super $_COOKIE: array<string>; 
super $_FILES: array<array<string>>; 
super $_ENV: array<string>; 
super $_REQUEST: array<any>; 
super $_SESSION: array<any>;

const PHP_SESSION_ACTIVE: number;
const PHP_SESSION_NONE: number;

const true: boolean;
const false: boolean;

const NULL: any; 
const null: any; 
const __FILE__: string;
const __DIR__: string;

const PHP_VERSION: string;
const PHP_OS: string;
const PHP_EOL: string;

function ini_set($key: string, $value: string);
function session_start();
function session_register($name: string);
function session_status(): number;
function session_destroy();
function session_id(): string;

function header($header: string);
function glob($path: string): array<string>;
function dirname($path: string): string;

function error_reporting($code: number);

function char_at($s: string, $pos: number): string;

function is_scalar($x: any): boolean;
function is_float($x: any): boolean;
function is_null($x: any): boolean;
function is_string($x: any): boolean;
function is_numeric($x: any): boolean;
function is_int($x: any): boolean;
function is_array($x: any): boolean;
function is_callable($x: any): boolean;
function is_bool($x: any): boolean;
function is_resource($x: any): boolean;
function is_file($name: string): boolean;
function is_object($var: any): boolean;
function is_a($object: any, $class_name: string, $allow_string?: boolean): boolean;

function max($a: array<number>): number;
function max($a: number, $b: number): number;

function min($a: array<number>): number;
function min($a: number, $b: number): number;

function floatval($a: string): number;
function intval($a: string, $radix?: number): number;

function join($sep: string, $list: array<any>): string;

function time(): number;
function file_exists($name: string): boolean;

function file_get_contents($file: string): string | boolean[false];
function file_put_contents($file: string, $data: string);
function filemtime($file: string): number;

function hexdec($hex_string: string): number;

class stdClass {
}

class Exception {

    function __construct ($message?: string, $code?: int, $previous?: Exception);

    function getMessage(): string;
    function getPrevious(): Exception;
    function getCode(): any;
    function getFile(): string;
    function getLine(): number;
    function getTrace(): array<string>;
    function getTraceAsString(): string;
    function __toString(): string;
}

class ErrorException {
}

function set_error_handler($fn: string);

function get_magic_quotes_gpc(): boolean;

function json_encode($value: any, $options?: number): string;
function print_r($value: any);
function iconv($from: string, $to: string, $source: string): string;
function chmod($filename: string, $mode: number);
function filesize($filename: string): number;

function date($format: string, $time?: number): string;

function http_build_query($data: array<string>): string;
function srand($seed: number);
function mt_srand($seed: number);
function microtime($option?: boolean): string | number;

function uniqid($prefix?: string, $more_entropy?: boolean): string;

function mktime(hour?: number, minute?: number, second?: number, month?: number, day?: number, year?: number, is_dst?: number): number;
function debug_backtrace(): array<string>;

function function_exists($name: string): boolean;
function md5($source: string,  $raw_output?: boolean): string;
function md5_file($filename: string): string;
function sha1($text: string): string;

function htmlspecialchars($string: string, $flags?: number, $encoding?: string, $double_encode?: boolean): string;
function htmlspecialchars_decode($string: string, $flags?: number): string;
function html_entity_decode($string: string , $flags?: int, $encoding?: string): string;

function checkdate(month: number, day: number, year: number): boolean;
function setcookie($name: string, $value?: string,  $expire?: number, $path?: string, $domain?:string, $secure?: boolean, $httponly?:boolean):boolean;
function chunk_split ($body: string, $chunklen?: int, $end?: string): string;
function ob_clean();

const CRYPT_BLOWFISH: number;
function crypt ($str: string, $salt?:string): string;
function openssl_random_pseudo_bytes ($length: number, $crypto_strong: boolean): string;
function sprintf ($format: string, $x1?: any, $x2?: any, $x3?: any, $x4?: any, $x5?: any, $x6?: any ): string ;

function ord($string: string): number;

function mt_rand (): number;
function mt_rand ($min: number, $max: number): number;

class ZipArchive {
    function getFromName($path: string): string;
    function open($file: string): number | boolean[true];
    function addEmptyDir($dirname: string): boolean;
    function extractTo ($destination: string, $entries?: any): boolean;
}

function is_dir($path: string): boolean;
function addslashes($s: string): string;
function ucfirst($s: string): string;
function parse_str($encoded_string: string, $result?: array<any>);

function boolval($var: any): boolean;
function strip_tags($str: string,  $allowable_tags?:string): string;

function fsockopen ($hostname: string, $port?: number, $errno?: number, $errstr?: string, $timeout?: number): PhpResource;
function ob_start($output_callback?: array<any>, $chunk_size?: number, $flags?: number): boolean;
function ob_get_contents(): string;

function getenv($name: string): string;

function soundex($str: string): string;
function strnatcmp($str1: string, $str2: string): number;
function ltrim ($str: string, $character_mask?: string): string;

function ftell($handle: PhpResource): number;
function set_time_limit($time: number);
function flush();
function mime_content_type($path: string): string;
function proc_nice($increment: number);
function ob_end_clean();
function bin2hex ($string: string) : string;
function hex2bin ($string: string) : string | boolean;
