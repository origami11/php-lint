
const FILEINFO_COMPRESS: number;
const FILEINFO_CONTINUE: number;
const FILEINFO_DEVICES: number;
const FILEINFO_MIME: number;
const FILEINFO_MIME_ENCODING: number;
const FILEINFO_MIME_TYPE: number;
const FILEINFO_NONE: number;
const FILEINFO_PRESERVE_ATIME: number;
const FILEINFO_RAW: number;
const FILEINFO_SYMLINK: number;

class finfo {
    constructor(options?: number, magic_file?: string);
    buffer(string_?: string, options?: number, context?: Pct.PhpResource): string;
    file(file_name?: string, options?: number, context?: Pct.PhpResource): string;
    set_flags(options: number): bool;
}

function finfo_buffer(finfo: Pct.PhpResource, string_?: string, options?: number, context?: Pct.PhpResource): string;
function finfo_close(finfo: Pct.PhpResource): bool;
function finfo_file(finfo: Pct.PhpResource, file_name?: string, options?: number, context?: Pct.PhpResource): string;
function finfo_open(options?: number, magic_file?: string): Pct.PhpResource;
function finfo_set_flags(finfo: Pct.PhpResource, options: number): bool;
function mime_content_type(filename: string): string;