
const INF: number;
const NAN: number;

const M_1_PI: number;
const M_2_PI: number;
const M_2_SQRTPI: number;
const M_E: number;
const M_EULER: number;
const M_LN10: number;
const M_LN2: number;
const M_LNPI: number;
const M_LOG10E: number;
const M_LOG2E: number;
const M_PI: number;
const M_PI_2: number;
const M_PI_4: number;
const M_SQRT1_2: number;
const M_SQRT2: number;
const M_SQRT3: number;
const M_SQRTPI: number;

const PHP_ROUND_HALF_DOWN: number;
const PHP_ROUND_HALF_EVEN: number;
const PHP_ROUND_HALF_ODD: number;
const PHP_ROUND_HALF_UP: number;

function rand(): number;
function rand($a: number, $b: number): number;

function round($path: number, $precision?: number, $mode?: number): number;
function sqrt($x: number): number;
function log($x: number): number;
function pi(): number;
function sin($x: number): number;
function cos($x: number): number;
function atan($x: number): number;
function asin($x: number): number;
function acos($x: number): number;
function tan($x: number): number;
function floor($x: number): number;
function ceil($x: number): number;
function pow($base: number, $exp: number): number;
function abs($number: number): number;
function mt_getrandmax(): number;
