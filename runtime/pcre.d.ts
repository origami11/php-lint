
const PCRE_VERSION: number;
const PREG_BACKTRACK_LIMIT_ERROR: number;
const PREG_BAD_UTF8_ERROR: number;
const PREG_BAD_UTF8_OFFSET_ERROR: number;
const PREG_INTERNAL_ERROR: number;
const PREG_NO_ERROR: number;
const PREG_OFFSET_CAPTURE: number;
const PREG_PATTERN_ORDER: number;
const PREG_RECURSION_LIMIT_ERROR: number;
const PREG_SET_ORDER: number;
const PREG_SPLIT_DELIM_CAPTURE: number;
const PREG_SPLIT_NO_EMPTY: number;
const PREG_SPLIT_OFFSET_CAPTURE: number;

function preg_replace($rx: string | array<string>, $from: string | array<string>, $str: string | array<string>): string | array<string>;
function preg_split($pattern: string, $subject: string, $limit?: number, $flags?: number): array<string>;
function preg_quote($str: string, $delimiter: string): string;
function preg_match($pattern: string, $subject: string, $matches?: array<any>, $flags?: number, $offset?: number): boolean; 
function preg_match_all($pattern: string, $subject: string, $matches?: array<any>, $flag?: number, $offset?: number): number; 

function preg_replace_callback ($pattern: string | array<string>, $callback: any, $subject: string | array<string> , $limit?: number , $count?: number): string | array<string>;

function mb_ereg_replace ($pattern: string, $replacement: string, $string: string , $option?: string): string;