
const CAL_GREGORIAN: number;
const CAL_JULIAN: number;
const CAL_JEWISH: number;
const CAL_FRENCH: number;

const CAL_DOW_DAYNO: number;
const CAL_DOW_SHORT: number;
const CAL_DOW_LONG: number;

const CAL_MONTH_GREGORIAN_SHORT: number;
const CAL_MONTH_GREGORIAN_LONG: number;
const CAL_MONTH_JULIAN_SHORT: number;
const CAL_MONTH_JULIAN_LONG: number;
const CAL_MONTH_JEWISH: number;
const CAL_MONTH_FRENCH: number;

const CAL_EASTER_DEFAULT: number;
const CAL_EASTER_ROMAN: number;
const CAL_EASTER_ALWAYS_GREGORIAN: number;
const CAL_EASTER_ALWAYS_JULIAN: number;

function cal_days_in_month(calendar: number, month: number, year: number): number;
function cal_from_jd(jd: number, calendar: number): array<any>;
function cal_info(calendar?: number): array<any>;
function cal_to_jd(calendar: number, month: number, day: number, year: number): number;
function easter_date(year?: number): number;
function easter_days(year?: number, method?: number): number;
function frenchtojd(month: number, day: number, year: number): number;
function gregoriantojd(month: number, day: number, year: number): number;
function jddayofweek(julianday: number, mode?: number): any;
function jdmonthname(julianday: number, mode: number): string;
function jdtofrench(juliandaycount: number): string;