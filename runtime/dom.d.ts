
const DOM_PHP_ERR: number;
const DOM_INDEX_SIZE_ERR: number;
const DOMSTRING_SIZE_ERR: number;
const DOM_HIERARCHY_REQUEST_ERR: number;
const DOM_WRONG_DOCUMENT_ERR: number;
const DOM_INVALID_CHARACTER_ERR: number;
const DOM_NO_DATA_ALLOWED_ERR: number;
const DOM_NO_MODIFICATION_ALLOWED_ERR: number;
const DOM_NOT_FOUND_ERR: number;
const DOM_NOT_SUPPORTED_ERR: number;
const DOM_INUSE_ATTRIBUTE_ERR: number;
const DOM_INVALID_STATE_ERR: number;
const DOM_SYNTAX_ERR: number;
const DOM_INVALID_MODIFICATION_ERR: number;
const DOM_NAMESPACE_ERR: number;
const DOM_INVALID_ACCESS_ERR: number;
const DOM_VALIDATION_ERR: number;

const XML_ELEMENT_NODE: number;
const XML_ATTRIBUTE_NODE: number;
const XML_TEXT_NODE: number;
const XML_CDATA_SECTION_NODE: number;
const XML_ENTITY_REF_NODE: number;
const XML_ENTITY_NODE: number;
const XML_PI_NODE: number;
const XML_COMMENT_NODE: number;
const XML_DOCUMENT_NODE: number;
const XML_DOCUMENT_TYPE_NODE: number;
const XML_DOCUMENT_FRAG_NODE: number;
const XML_NOTATION_NODE: number;
const XML_HTML_DOCUMENT_NODE: number;
const XML_DTD_NODE: number;
const XML_ELEMENT_DECL_NODE: number;
const XML_ATTRIBUTE_DECL_NODE: number;
const XML_ENTITY_DECL_NODE: number;
const XML_NAMESPACE_DECL_NODE: number;
const XML_ATTRIBUTE_CDATA: number;
const XML_ATTRIBUTE_ID: number;
const XML_ATTRIBUTE_IDREF: number;
const XML_ATTRIBUTE_IDREFS: number;
const XML_ATTRIBUTE_ENTITY: number;
const XML_ATTRIBUTE_NMTOKEN: number;
const XML_ATTRIBUTE_NMTOKENS: number;
const XML_ATTRIBUTE_ENUMERATION: number;
const XML_ATTRIBUTE_NOTATION: number;

class DOMNode {
    var nodeName: string;
    var nodeValue: string;
    var nodeType: number;
    var parentNode: DOMNode;
    var childNodes: DOMNodeList;
    var firstChild: DOMNode;
    var lastChild: DOMNode;
    var previousSibling: DOMNode;
    var nextSibling: DOMNode;
//    var attributes: DOMNamedNodeMap;
    var ownerDocument: DOMDocument;
    var namespaceURI: string;
    var prefix: string;
    var localName: string;
    var baseURI: string;
    var textContent: string;

    function appendChild($newnode: DOMNode): DOMNode;
    function C14N($exclusive?: boolean, $with_comments?: boolean, $xpath?: array<string>, $ns_prefixes?: array<string>): number;
    function C14NFile($uri: string, $exclusive?: boolean, $with_comments?: boolean, $xpath?: array<string>, $ns_prefixes?: array<string>): string;
    function cloneNode($deep?: boolean): DOMNode;
    function getLineNo(): number;
    function getNodePath(): string;
    function hasAttributes(): boolean;
    function hasChildNodes(): boolean;
    function insertBefore($newnode: DOMNode, $refnode?: DOMNode): DOMNode;
    function isDefaultNamespace($namespaceURI: string): boolean;
    function isSameNode($node: DOMNode): boolean;
    function isSupported($feature: string, $version: string): boolean;
    function lookupNamespaceURI($prefix: string): string;
    function lookupPrefix($namespaceURI: string): string;
    function normalize();
    function removeChild($oldnode: DOMNode): DOMNode;
    function replaceChild($newnode: DOMNode, $oldnode: DOMNode): DOMNode;
}

class DOMNodeList extends Iterator {
    var length: number;

    function item($index: number): DOMNode;
}

class DOMAttr extends DOMNode {
    var ownerElement: DOMElement;
    var schemaTypeInfo: boolean;
    var specified: boolean;
    var value: string;

//    constructor(name: string, value?: string);
//    isId(): boolean;
}

/*
class DOMCharacterData extends DOMNode {
    data: string;
    length: number;

    appendData(data: string);
    deleteData(offset: number, count: number);
    insertData(offset: number, data: string);
    replaceData(offset: number, count: number, data: string);
    substringData(offset: number, count: number): string;
}

class DOMText extends DOMCharacterData {
    wholeText: string;

    constructor(value?: string);
    isWhitespaceInElementContent(): boolean;
    splitText(offset: number): DOMText;
}

class DOMCdataSection extends DOMText {
    constructor(value: string);
}

class DOMComment extends DOMCharacterData {

} */

class DOMDocument extends DOMNode {

//    function load(filename: string, options?: number): DOMDocument;
//    function loadHTML(source: string): DOMDocument;
//    function loadHTMLFile(filename: string): DOMDocument;
//    function loadXML(source: string, options?: number): DOMDocument;

    var actualEncoding: string;
//    var doctype: DOMDocumentType;
//    var documentElement: DOMElement;
    var documentURI: string;
    var encoding: string;
    var formatOutput: boolean;
//    var implementation: DOMImplementation;
    var preserveWhiteSpace: boolean;
    var recover: boolean;
    var resolveExternals: boolean;
    var standalone: boolean;
    var strictErrorChecking: boolean;
    var substituteEntities: boolean;
    var validateOnParse: boolean;
    var version: string;
    var xmlEncoding: string;
    var xmlStandalone: boolean;
    var xmlVersion: string;

    function __construct(version?: string, encoding?: string);
//    function createAttribute(name: string): DOMAttr;
//    function createAttributeNS(namespaceURI: string, qualifiedName: string): DOMAttr;
//    function createCDATASection(data: string): DOMCdataSection;
    function createComment(data: string);
//    function createDocumentFragment(): DOMDocumentFragment;
//    function createElement(name: string, value?: string): DOMElement;
//    function createElementNS(namepsaceURI: string, qualifiedName: string, value?: string): DOMElement;
//    function createEntityReference(name: string): DOMEntityReference;
//    function createProcessingInstruction(target: string, data?: string): DOMProcessingInstruction;
//    function createTextNode(context: string): DOMText;
//    function getElementById(elementId: string): DOMElement;
    function getElementsByTagName($name: string): DOMNodeList;
//    function getElementsByTagName(namespaceURI: string, localName: string): DOMNodeList;
//    function importNode(importedNode: DOMNode, deep?: boolean): DOMNode;
    function load($filename: string, $options?: number): boolean;
    function loadHTML($source: string): boolean;
    function loadHTMLFile($filename: string): boolean;
    function loadXML($source: string, $options?: number): boolean;
    function normalizeDocument();
    function registerNodeClass($baseclass: string, $extendedclass: string): boolean;
    function relaxNGValidate($filename: string): boolean;
    function relaxNGValidateSource($source: string): boolean;
    function save($filename: string, $options?: number): number;
    function saveHTML($node?: DOMNode): string;
    function saveHTMLFile($filename: string): number;
    function saveXML(node?: DOMNode, $options?: number): string;
    function schemaValidate($filename: string): boolean;
    function schemaValidateSource($source: string): boolean;
    function validate(): boolean;
    function xinclude($options?: number): number;
}

/*class DOMDocumentFragment extends DOMNode {
    appendXML(data: string): boolean;
} 

class DOMDocumentType extends DOMNode {
    publicId: string;
    systemId: string;
    name: string;
    entities: DOMNamedNodeMap;
    notations: DOMNamedNodeMap;
    internalSubset: string;
}*/

class DOMElement extends DOMNode {
    var schemaTypeInfo: boolean;
    var tagName: string;

    function __construct($name: string, $value?: string, $namespaceURI?: string);
    function getAttribute($name: string): string;
//    getAttributeNode(name: string): DOMAttr;
//    getAttributeNodeNS(namespaceURI: string, localName: string): DOMAttr;
//    getAttributeNS(namespaceURI: string, localName: string): string;
    function getElementsByTagName(name: string): DOMNodeList;
//    getElementsByTagNameNS(namespaceURI: string, localName: string): DOMNodeList;
    function hasAttribute($name: string): boolean;
//    hasAttributeNS(namespaceURI: string, localName: string): boolean;
    function removeAttribute($name: string): boolean;
//    removeAttributeNode(oldnode: DOMAttr): boolean;
//    removeAttributeNS(namespaceURI: string, localName: string): boolean;
    function setAttribute($name: string, $value: string): DOMAttr;
//    setAttributeNode(attr: DOMAttr): DOMAttr;
//    setAttributeNodeNS(attr: DOMAttr): DOMAttr;
//    setAttributeNS(namespaceURI: string, qualifiedName: string, value: string);
//    setIdAttribute(name: string, isId: boolean);
//    setIdAttributeNode(attr: DOMAttr, isId: boolean);
//    setIdAttributeNS(namespaceURI: string, localName: string, isId: boolean);
}

/*
class DOMEntity extends DOMNode {
    publicId: string;
    systemId: string;
    notationName: string;
    actualEncoding: string;
    encoding: string;
    version: string;
}

class DOMEntityReference extends DOMNode {
    constructor(name: string);
}

class DOMException extends Exception {
    code: number;
}

class DOMImplementation {
    createDocument(namespaceURI?: string, qualfiiedName?: string, doctype?: DOMDocumentType): DOMDocument;
    createDocumentType(qualifiedName?: string, publicId?: string, systemId?: string): DOMDocumentType;
    hasFeature(feature: string, version: string): boolean;
}

class DOMNamedNodeMap implements Traversable {
    length: number;

    getNamedItem(name: string): DOMNode;
    getNamedItemNS(namespaceURI: string, localName: string): DOMNode;
    item(index: number): DOMNode;
}

class DOMNotation extends DOMNode {
    publicId: string;
    systemId: string;
}

class DOMProcessingInstruction extends DOMNode {
    target: string;
    data: string;

    constructor(name: string, value?: string);
}

class DOMXPath {
    document: DOMDocument;

    constructor(doc: DOMDocument);
    evaluate(expression: string, contextnode?: DOMNode, registerNodeNS?: boolean): any;
    query(expression: string, contextnode?: DOMNode, registerNodeNS?: boolean): DOMNodeList;
    registerNamespace(prefix: string, namespaceURI: string): boolean;
    registerPhpFunctions(restrict?: string);
    registerPhpFunctions(restrict: array<string>);
}

function dom_import_simplexml(node: SimpleXMLElement): DOMElement;
*/
