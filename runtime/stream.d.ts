
const PSFS_ERR_FATAL: number;
const PSFS_FEED_ME: number;
const PSFS_FLAG_FLUSH_CLOSE: number;
const PSFS_FLAG_FLUSH_INC: number;
const PSFS_FLAG_NORMAL: number;
const PSFS_PASS_ON: number;

const STREAM_CAST_AS_STREAM: number;
const STREAM_CAST_FOR_SELECT: number;

const STREAM_CLIENT_ASYNC_CONNECT: number;
const STREAM_CLIENT_CONNECT: number;
const STREAM_CLIENT_PERSISTENT: number;

const STREAM_FILTER_ALL: number;
const STREAM_FILTER_READ: number;
const STREAM_FILTER_WRITE: number;

const STREAM_IPPROTO_ICMP: number;
const STREAM_IPPROTO_IP: number;
const STREAM_IPPROTO_RAW: number;
const STREAM_IPPROTO_TCP: number;
const STREAM_IPPROTO_UDP: number;

const STREAM_META_ACCESS: number;
const STREAM_META_GROUP: number;
const STREAM_META_GROUP_NAME: number;
const STREAM_META_OWNER: number;
const STREAM_META_OWNER_NAME: number;
const STREAM_META_TOUCH: number;

const STREAM_NOTIFY_AUTH_REQUIRED: number;
const STREAM_NOTIFY_AUTH_RESULT: number;
const STREAM_NOTIFY_COMPLETED: number;
const STREAM_NOTIFY_CONNECT: number;
const STREAM_NOTIFY_FAILURE: number;
const STREAM_NOTIFY_FILE_SIZE_IS: number;
const STREAM_NOTIFY_MIME_TYPE_IS: number;
const STREAM_NOTIFY_PROGRESS: number;
const STREAM_NOTIFY_REDIRECTED: number;
const STREAM_NOTIFY_RESOLVE: number;
const STREAM_NOTIFY_SEVERITY_ERR: number;
const STREAM_NOTIFY_SEVERITY_INFO: number;
const STREAM_NOTIFY_SEVERITY_WARN: number;

const STREAM_PF_INET6: number;
const STREAM_PF_INET: number;
const STREAM_PF_UNIX: number;

const STREAM_REPORT_ERRORS: number;

const STREAM_SERVER_BIND: number;
const STREAM_SERVER_LISTEN: number;

const STREAM_SHUT_RD: number;
const STREAM_SHUT_RDWR: number;
const STREAM_SHUT_WR: number;

const STREAM_SOCK_DGRAM: number;
const STREAM_SOCK_RAW: number;
const STREAM_SOCK_RDM: number;
const STREAM_SOCK_SEQPACKET: number;
const STREAM_SOCK_STREAM: number;

const STREAM_USE_PATH: number;

/*class php_user_filter {
    filtername: string;
    params: any; //TODO: wat?

    filter(in_: PhpResource, out: PhpResource, $consumed: number, closing: boolean): number;
    onClose();
    onCreate(): boolean;
}

//TODO: streamWrapper an interface or class?

function stream_bucket_append(brigade: PhpResource, bucket: PhpResource);
function stream_bucket_make_writable(brigade: PhpResource);
function stream_bucket_new(stream: PhpResource, buffer: string): any;
function stream_bucket_prepend(brigade: PhpResource, bucket: PhpResource);
function stream_context_create(options?: PhpAssocArray, params?: PhpAssocArray): PhpResource;
function stream_context_get_default(options?: PhpAssocArray): PhpResource;
function stream_context_get_options(stream_or_context: PhpResource): PhpAssocArray;
function stream_context_get_params(stream_or_context: PhpResource): PhpAssocArray;
function stream_context_set_default(options: PhpAssocArray): PhpResource;
function stream_context_set_option(stream_or_context: PhpResource, wrapper: string, option: string, value: any): boolean;
function stream_context_set_option(stream_or_context: PhpResource, options: PhpAssocArray): boolean;
function stream_context_set_params(stream_or_context: PhpResource, params: PhpAssocArray): boolean;
*/
function stream_copy_to_stream($source: PhpResource, $dest: PhpResource, $maxlength?: $number, $offset?: number): number;
/*function stream_encoding(stream: PhpResource, encoding?: string): boolean;
function stream_filter_append(stream: PhpResource, filtername: string, read_write?: number, params?: PhpAssocArray): PhpResource;
function stream_filter_prepend(stream: PhpResource, filtername: string, read_write?: number, params?: PhpAssocArray): PhpResource;
function stream_filter_register(filtername: string, classname: string): boolean;
function stream_filter_remove(stream_filter: PhpResource): boolean;
function stream_get_contents(handle: PhpResource, maxlength?: number, offset?: number): string;
function stream_get_filters(): string[];
function stream_get_line(handle: PhpResource, length: number, ending?: string): string;
function stream_get_meta_data(stream: PhpResource): PhpAssocArray;
function stream_get_transports(): string[];
function stream_get_wrappers(): string[];
function stream_is_local(url: string): boolean;
function stream_is_local(stream: PhpResource): boolean;
function stream_resolve_include_path(filename: string): string;
function stream_select($read: PhpResource[], $write: PhpResource[], $except: PhpResource[], tv_sec: number, tv_usec?: number): number;
function stream_set_blocking(stream: PhpResource, mode: number): boolean;
function stream_set_chunk_size(fp: PhpResource, chunk_size: number): number;
function stream_set_read_buffer(stream: PhpResource, buffer: number): number;
function stream_set_timeout(stream: PhpResource, seconds: number, microseconds?: number): boolean;
function stream_set_write_buffer(stream: PhpResource, buffer: number): number;
function stream_socket_accept(server_socket: PhpResource, timeout?: number, $peername?: string): PhpResource;
function stream_socket_client(remote_socket: string, $errno?: number, $errstr?: string, timeout?: number, flags?: number, context?: PhpResource): PhpResource;
function stream_socket_enable_crypto(stream: PhpResource, enable: boolean, crypto_type?: number, session_stream?: PhpResource): any;
function stream_socket_get_name(handle: PhpResource, want_peer: boolean): string;
function stream_socket_pair(domain: number, type: number, protocol: number): PhpResource[];
function stream_socket_recvfrom(socket: PhpResource, length: number, flags?: number, $address?: string): string;
function stream_socket_sendto(socket: PhpResource, data: string, flags?: number, address?: string): number;
function stream_socket_server(local_socket: string, $errno?: number, $errstr?: string, flags?: number, context?: PhpResource): PhpResource;
function stream_socket_shutdown(stream: PhpResource, how: number): boolean;
function stream_supports_lock(stream: PhpResource): boolean;
function stream_wrapper_register(protocol: string, classname: string, flags?: number): boolean;
function stream_wrapper_restore(protocol: string): boolean;
function stream_wrapper_unregister(protocol: string): boolean;
*/