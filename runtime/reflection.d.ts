/*
interface Reflector {
    //static export(): string; http://typescript.codeplex.com/workitem/80

}*/

class Reflection {
    //static export(): string; http://typescript.codeplex.com/workitem/80
//    static getModifierNames(modifiers: number): string[];
}

class ReflectionClass /*implements Reflector*/ {
//    static IS_IMPLICIT_ABSTRACT: number;
//    static IS_EXPLICIT_ABSTRACT: number;
//    static IS_FINAL: number;

    var name: string;

    function constructor(argument: string);
  //  constructor(argument: Object);

//    getConstant(name: string): any;
//    getConstants(): Pct.PhpAssocArray;
//    getConstructor(): ReflectionMethod;
//    getDefaultProperties(): Pct.PhpAssocArray;
//    getDocComment(): string;
//    getEndLine(): number;
//     getExtension(): ReflectionExtension;
//     getExtensionName(): string;
//     getFileName(): string;
//     getInterfaceNames(): string[];
//     getInterfaces(): Pct.PhpAssocArray;
//     getMethod(name: string): ReflectionMethod;
//     getMethods(filter?: number): ReflectionMethod[];
//     getModifiers(): number;
//     getName(): string;
//     getNamespaceName(): string;
//     getParentClass(): ReflectionClass;
//     getProperties(filter?: number): ReflectionProperty[];
//     getProperty(name: string): ReflectionProperty;
//     getShortName(): string;
//     getStartLine(): number;
//     getStaticProperties(): Pct.PhpAssocArray;
//     getStaticPropertyValue(name: string): any;
//     getTraitAliases(): Pct.PhpAssocArray;
//     getTraitNames(): string[];
//     getTraits(): Pct.PhpAssocArray;
//     hasConstant(name: string): boolean;
//     hasMethod(name: string): boolean;
//     hasProperty(name: string): boolean;
//     implementsInterface(interface_: string): boolean;
//     inNamespace(): boolean;
//     isAbstract(): boolean;
//     isCloneable(): boolean;
//     isFinal(): boolean;
//     isInstance(object: any): boolean;
//     isInstantiable(): boolean;
//     isInterface(): boolean;
//     isInternal(): boolean;
//     isIterateable(): boolean;
//     isSubclassOf(class_: string): boolean;
//     isTrait(): boolean;
//     isUserDefined(): boolean;
//     newInstance(...args: any[]): any;
       function newInstanceArgs(args?: array<any>): any;
//     newInstanceWithoutConstructor(): any;
//     setStaticPropertyValue(name: string, value: any);
}

/*
class ReflectionZendExtension implements Reflector {
    name: string;

    constructor(name: string);
    getAuthor(): string;
    getCopyright(): string;
    getName(): string;
    getURL(): string;
    getVersion(): string;
}

class ReflectionExtension implements Reflector {
    name: string;

    constructor(name: string);
    getClasses(): Pct.PhpAssocArray;
    getClassNames(): string[];
    getConstants(): Pct.PhpAssocArray;
    getDependencies(): Pct.PhpAssocArray;
    getFunctions(): Pct.PhpAssocArray;
    getINIEntries(): Pct.PhpAssocArray;
    getName(): string;
    getVersion(): string;
    info();
    isPersistent(): boolean;
    isTemporary(): boolean;
}*/

class ReflectionFunctionAbstract /*implements Reflector*/ {
    var name: string;

    function getClosureThis(): any;
    function getDocComment(): string;
    function getEndLine(): number;
//    getExtension(): ReflectionExtension;
    function getExtensionName(): string;
    function getFileName(): string;
    function getName(): string;
    function getNamespaceName(): string;
    function getNumberOfParameters(): number;
    function getNumberOfRequiredParameters(): number;
    function getParameters(): array<ReflectionParameter>;
    function getShortName(): string;
    function getStartLine(): number;
    function getStaticVariables(): array<any>; 
    function inNamespace(): boolean;
    function isClosure(): boolean;
    function isDeprecated(): boolean;
    function isInternal(): boolean;
    function isUserDefined(): boolean;
    function returnsReference(): boolean;
}

/*class ReflectionFunction extends ReflectionFunctionAbstract {
    static IS_DEPRECATED: number;

    name: string;

    constructor(name: any);
    getClosure(): Closure;
    invoke(...parameter: any[]): any;
    invokeArgs(args: any[]): any;
    isDisabled(): boolean;
}*/

class ReflectionMethod extends ReflectionFunctionAbstract {
//    static IS_STATIC: number;
//    static IS_PUBLIC: number;
//    static IS_PROTECTED: number;
//    static IS_PRIVATE: number;
//    static IS_ABSTRACT: number;
//    static IS_FINAL: number;

    var name: string;
    var class_: string;

    function constructor($class_: string, name: string);
    function getClosure($object?: any): Closure;
//    getDeclaringClass(): ReflectionClass;
    function getModifiers(): number;
    function getPrototype(): ReflectionMethod;
//    invoke($object: any, ...parameter: any[]): any;
//    invokeArgs($object: any, args: any[]): any;
    function isAbstract(): boolean;
    function isConstructor(): boolean;
    function isDestructor(): boolean;
    function isFinal(): boolean;
    function isPrivate(): boolean;
    function isProtected(): boolean;
    function isPublic(): boolean;
    function isStatic(): boolean;
    function setAccessible($accessible: boolean);
}

/*
class ReflectionObject extends ReflectionClass {
}
*/

class ReflectionParameter /*implements Reflector*/ {
    var name: string;

    function constructor($function_: string, $parameter: string);
    function allowsNull(): boolean;
    function canBePassedByValue(): boolean;
//    getClass(): ReflectionClass;
//    getDeclaringClass(): ReflectionClass;
//    getDeclaringFunction(): ReflectionFunction;
//    getDefaultValue(): any;
//    getName(): string;
//    getPosition(): number;
//    isArray(): boolean;
//    isDefaultValueAvailable(): boolean;
//    isOptional(): boolean;
//    isPassedByReference(): boolean;
}

/*
class ReflectionProperty implements Reflector {
    static IS_STATIC: number;
    static IS_PUBLIC: number;
    static IS_PROTECTED: number;
    static IS_PRIVATE: number;

    name: string;
    class_: string;

    constructor(class_: string, name: string);
    constructor(class_: Object, name: string);
    getDeclaringClass(): ReflectionClass;
    getDocComment(): string;
    getModifiers(): number;
    getName(): string;
    getValue(object?: any): any;
    isDefault(): boolean;
    isPrivate(): boolean;
    isProtected(): boolean;
    isPublic(): boolean;
    isStatic(): boolean;
    setAccessible(accessible: boolean);
    setValue(value: any);
    setValue(object: any, value: any);
}*/

class ReflectionException extends Exception { }
