
const ASSERT_ACTIVE: number;
const ASSERT_WARNING: number;
const ASSERT_BAIL: number;
const ASSERT_QUIET_EVAL: number;
const ASSERT_CALLBACK: number;

const CREDITS_ALL: number;
const CREDITS_DOC: number;
const CREDITS_FULLPAGE: number;
const CREDITS_GENERAL: number;
const CREDITS_GROUP: number;
const CREDITS_MODULES: number;
const CREDITS_SAPI: number;

const INFO_GENERAL: number;
const INFO_CREDITS: number;
const INFO_CONFIGURATION: number;
const INFO_MODULES: number;
const INFO_ENVIRONMENT: number;
const INFO_VARIABLES: number;
const INFO_LICENSE: number;
const INFO_ALL: number;

//function assert_options(what: number, value?: any): any;
function assert($assertion: boolean, $description?: string): boolean;
//function assert(assertion: string, description?: string): boolean;
function dl($library: string): boolean;
function extension_loaded($name: string): boolean;
function gc_collect_cycles(): number;
function gc_disable();
function gc_enable();
//function gc_enabled(): boolean;
//function get_cfg_var(option: string): string;
//function get_current_user(): string;
//function get_defined_constants(categorize?: bool): Pct.PhpAssocArray;
//function get_extension_funcs(module_name: string): string[];
function get_include_path(): string;
//function get_included_files(): string[];
//function get_loaded_extensions(zend_extensions?: bool): string[];
//function getenv(varname: string): string;
//function getlastmod(): number;
function getmygid(): number;
function getmyinode(): number;
function getmypid(): number;
function getmyuid(): number;
//function getopt(options: string, longopts?: Array): Pct.PhpAssocArray;
//function getrusage(who?: number): Pct.PhpAssocArray;
//function ini_get(varname: string): string;
//function ini_get_all(extension?: string, details?: bool): Pct.PhpAssocArray;
//function ini_restore(varname: string);
//function ini_set(varname: string, newvalue: string): string;
//function memory_get_peak_usage(real_usage?: bool): number;
//function memory_get_usage(real_usage?: bool): number;
function php_ini_loaded_file(): string;
function php_ini_scanned_files(): string;
function php_logo_guid(): string;
function php_sapi_name(): string;
function php_uname($mode?: string): string;
//function phpcredits(flag?: number): boolean;
//function phpinfo(what?: number): boolean;
//function phpversion(extension?: string): string;
//function putenv(setting: string): boolean;
//function restore_include_path();
function set_include_path($new_include_path: string): string;
//function set_time_limit(seconds: number);
//function sys_get_temp_dir(): string;
function version_compare($version1: string, $version2: string, $operator: string): boolean;
//function zend_logo_guid(): string;
//function zend_thread_id(): number;
//function zend_version(): string;