
const JSON_ERROR_CTRL_CHAR: number;
const JSON_ERROR_DEPTH: number;
const JSON_ERROR_NONE: number;
const JSON_ERROR_STATE_MISMATCH: number;
const JSON_ERROR_SYNTAX: number;
const JSON_ERROR_UTF8: number;

const JSON_BIGINT_AS_STRING: number;
const JSON_FORCE_OBJECT: number;
const JSON_HEX_AMP: number;
const JSON_HEX_APOS: number;
const JSON_HEX_QUOT: number;
const JSON_HEX_TAG: number;
const JSON_NUMERIC_CHECK: number;
const JSON_PRETTY_PRINT: number;
const JSON_UNESCAPED_SLASHES: number;
const JSON_UNESCAPED_UNICODE: number;

/*interface JsonSerializable {
    jsonSerialize(): any;
}
*/

function json_decode($json: string, $assoc?: boolean, $depth?: number, $options?: number): any;
function json_encode($value: any, $options?: number): string;
function json_last_error(): number;