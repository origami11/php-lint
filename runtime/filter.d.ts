
const FILTER_CALLBACK: number;

const FILTER_DEFAULT: number;

const FILTER_FLAG_ALLOW_FRACTION: number;
const FILTER_FLAG_ALLOW_HEX: number;
const FILTER_FLAG_ALLOW_OCTAL: number;
const FILTER_FLAG_ALLOW_SCIENTIFIC: number;
const FILTER_FLAG_ALLOW_THOUSAND: number;
const FILTER_FLAG_EMPTY_STRING_NULL: number;
const FILTER_FLAG_ENCODE_AMP: number;
const FILTER_FLAG_ENCODE_HIGH: number;
const FILTER_FLAG_ENCODE_LOW: number;
const FILTER_FLAG_IPV4: number;
const FILTER_FLAG_IPV6: number;
const FILTER_FLAG_NO_ENCODE_QUOTES: number;
const FILTER_FLAG_NO_PRIV_RANGE: number;
const FILTER_FLAG_NO_RES_RANGE: number;
const FILTER_FLAG_NONE: number;
const FILTER_FLAG_PATH_REQUIRED: number;
const FILTER_FLAG_QUERY_REQUIRED: number;
const FILTER_FLAG_STRIP_HIGH: number;
const FILTER_FLAG_STRIP_LOW: number;

const FILTER_FORCE_ARRAY: number;

const FILTER_NULL_ON_FAILURE: number;

const FILTER_REQUIRE_ARRAY: number;
const FILTER_REQUIRE_SCALAR: number;

const FILTER_SANITIZE_EMAIL: number;
const FILTER_SANITIZE_ENCODED: number;
const FILTER_SANITIZE_MAGIC_QUOTES: number;
const FILTER_SANITIZE_NUMBER_FLOAT: number;
const FILTER_SANITIZE_NUMBER_INT: number;
const FILTER_SANITIZE_SPECIAL_CHARS: number;
const FILTER_SANITIZE_STRING: number;
const FILTER_SANITIZE_STRIPPED: number;
const FILTER_SANITIZE_URL: number;

const FILTER_UNSAFE_RAW: number;

const FILTER_VALIDATE_BOOLEAN: number;
const FILTER_VALIDATE_EMAIL: number;
const FILTER_VALIDATE_FLOAT: number;
const FILTER_VALIDATE_INT: number;
const FILTER_VALIDATE_IP: number;
const FILTER_VALIDATE_REGEXP: number;
const FILTER_VALIDATE_URL: number;

const INPUT_COOKIE: number;
const INPUT_ENV: number;
const INPUT_GET: number;
const INPUT_POST: number;
const INPUT_REQUEST: number;
const INPUT_SERVER: number;
const INPUT_SESSION: number;

//function filter_has_const($type: number, constiable_name: string): bool;
//function filter_id(filtername: string): number;
//function filter_input($type: number, constiable_name: string, filter?: number, options?: any): any;
//function filter_input_array(type: number, definition?: any): any;
//function filter_list(): string[];
function filter_var($constiable: any, $filter?: number, $options?: any): any;
//function filter_var_array(data: array<any>, definition?: any): any;
