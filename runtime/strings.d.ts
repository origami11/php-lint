
const HTML_ENTITIES: number;
const HTML_SPECIALCHARS: number;

const ENT_COMPAT: number;
const ENT_QUOTES: number;
const ENT_NOQUOTES: number;
const ENT_IGNORE: number;
const ENT_SUBSTITUTE: number;
const ENT_DISALLOWED: number;
const ENT_HTML401: number;
const ENT_XML1: number;
const ENT_XHTML: number;
const ENT_HTML5: number;

const LC_ALL: number;
const LC_COLLATE: number;
const LC_CTYPE: number;
const LC_MONETARY: number;
const LC_NUMERIC: number;
const LC_TIME: number;
const LC_MESSAGES: number;

const STR_PAD_RIGHT: number;
const STR_PAD_LEFT: number;
const STR_PAD_BOTH: number;

function strval($x: any): string;
function str_replace($from: array<string> | string, $to: array<string> | string, $base: string): string;
function strlen($src: string): number;
function substr($string: string, $start: number, $length?: number): string;
function trim($s: string, $character_mask?: string): string;
function rtrim($s: string): string;
function str_repeat($input: string, $multiplier: number):string;
function substr_count($haystack: string, $needle: string): number;
function realpath($path: string): string;

function ctype_space($text: string): boolean;
function ctype_digit($text: string): boolean;
function strpos(haystack: string, needle: string, $offset?: number): number | boolean[false];
function setlocale($category: number, $locale: string): string;
function strtoupper($source: string): string;
function strtolower($source: string): string;
function strtr($str: string, $x1?: array<string> | string, $x2?: string): string;

function implode($delimiter: string, $arr: array<string>): string;
function explode($delimiter: string, $string: string, $options?: number): array<string>;

function stripslashes($source: string): string;

function mb_strtolower($text: string, $encoding?: string): string;
function mb_strlen($text: string): number;
function mb_substr($text: string, $start: number, $length?: number, $encoding?: string): string;
function mb_strpos ($haystack: string , $needle: string , $offset?: number,  $encoding?: string): number | boolean[false];

function mb_internal_encoding($encoding: string);
function mb_detect_encoding ($str: string , $encoding_lis?: any , $strict?: boolean): string | boolean;

function strrpos($haystack: string, $needle: string, $offset?: number): number | boolean;
function strtok($tokstr: string, $token?: string): string;
function strcasecmp($str1: string, $str2:string): number;
function strcmp($str1: string, $str2:string): number;
function strrev($str1: string): string;
function chr($n: number): string;

