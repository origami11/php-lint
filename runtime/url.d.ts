
const PHP_QUERY_RFC1738: number;
const PHP_QUERY_RFC3986: number;

const PHP_URL_FRAGMENT: number;
const PHP_URL_HOST: number;
const PHP_URL_PASS: number;
const PHP_URL_PATH: number;
const PHP_URL_PORT: number;
const PHP_URL_QUERY: number;
const PHP_URL_SCHEME: number;
const PHP_URL_USER: number;

function base64_decode($data: string, $strict?: boolean): string;
function base64_encode($data: string): string;
function get_headers($url: string, $format?: number): array<string>;
//function get_meta_tags(filename: string, use_include_path?: bool): Pct.PhpAssocArray;
//function http_build_query(query_data: any, numeric_prefix?: string, arg_separator?: string, enc_type?: number): string;
//function parse_url(url: string): Pct.PhpAssocArray;
function parse_url($url: string, $component?: number): any;
function rawurldecode($str: string): string;
function rawurlencode($str: string): string;
function urldecode($str: string): string;
function urlencode($str: string): string;