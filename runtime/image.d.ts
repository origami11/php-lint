//
function imagecreate ($width: number, $height: number): resource;
function imagecolorat ($image: resource, $x: number, $y: number): number;
function imagesetpixel ($image: resource, $x: number, $y: number, $color: number);
function imagedestroy($image: resource);
function imagefill($image: resource, $x: number, $y: number, $color: number): boolean;
function imagecolorallocate($image: resource, $red: number, $green: number, $blue: number): number;
function imageline($image: resource, $x1: number, $y1: number, $x2: number, $y2: number, $color: number): boolean;
function imagepng($image: resource, $to?: any, $quality?: number, $filters?: number): boolean;
function imagecolorsforindex ($image: resource, $index:number): array<number>;
function imagettftext ($image: resource, $size: number, $angle: number, $x: number, $y: number, $color: number, $fontfile: string, $text: string): array<number>;
function imagefilledellipse ($image: resource, $cx: number, $cy: number, $width: number, $height: number, $color: number): boolean;

function imagecreatefrompng($filename: string): resource;
function imagecreatefromjpeg($filename: string): resource;
function imagecreatefromgif($filename: string): resource;
function imagesx($image: resource): number;
function imagesy($image: resource): number;
function imagecreatetruecolor($width: number, $height: number): resource;
function imagecopyresampled ($dst_image: resource, $src_image: resource, $dst_x: number, $dst_y: number, $src_x: number, $src_y: number, $dst_w: number, $dst_h: number, $src_w: number, $src_h: number ): boolean;
function imagejpeg($image: resource, $to?: any, $quality?: number): boolean;
function imagegif($image: resource, $to?: any): boolean;

function imagettfbbox($size: number, $angle: number, $fontfile: string, $text: string): array<any>;
function getimagesize ($filename: string, $image_info?: array ) : array|boolean;
