
const __LINE__: number;
const __FILE__: string;
const __DIR__: string;
const __FUNCTION__: string;
const __CLASS__: string;
const __TRAIT__: string;
const __METHOD__: string;
const __NAMESPACE__: string;

var $php_errormsg: string;
var $http_response_header: string[];
var $argc: number;
var $argv: string[];

function include(path: string): any;
function include_once(path: string): any;
function require(path: string): any;
function require_once(path: string): any;

interface Traversable {
    forEach?(callbackfn: (value: any, index: any) => void);
}

interface Iterator extends Traversable {
    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
}

interface IteratorAggregate extends Traversable {
    getIterator(): Traversable;
}

interface ArrayAccess extends Pct.Indexable {
    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
}

interface Serializable {
    serialize(): string;
    unserialize(serialized: string);
}

class Closure {
    static bind(closure: Closure, newthis: any, newscope?: any): Closure;

    constructor();
    bindTo(newthis: any, newscope?: any): Closure;
}

class stdClass {
}

class Exception implements Error {
    name: string; //only here for compat
    message: string;
    code: number;
    file: string;
    line: number;

    constructor(message?: string, code?: number, previous?: Exception);
    getMessage(): string;
    getPrevious(): Exception;
    getCode(): number;
    getFile(): string;
    getLine(): number;
    getTrace(): Pct.PhpAssocArray[];
    getTraceAsString(): string;
}

class ErrorException extends Exception {
    severity: number;

    constructor(message?: string, code?: number, severity?: number, filename?: string, lineno?: number, previous?: Exception);
}
