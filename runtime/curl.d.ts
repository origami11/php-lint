
const CURLOPT_URL: number;
const CURLOPT_RETURNTRANSFER: number;
const CURLOPT_PROXY: number;
const CURLOPT_USERAGENT: number;
const CURLOPT_AUTOREFERER: number;
const CURLOPT_RETURNTRANSFER: number;
const CURLOPT_VERBOSE: number;

class CurlResource {
}

function curl_init(): CurlResource;
function curl_setopt($curl: CurlResource, $option: number, $value: any);
function curl_exec($curl: CurlResource): string;