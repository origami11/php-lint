//my god are the SPL docs bad...wrong return types, param types, etc...

interface Traversable {
}

interface IteratorAggregate extends Traversable {
    function getIterator(): Traversable;
}

interface Iterator {
    function next(): boolean;
    function current(): any;
    function key(): any;
    function rewind();
    function valid(): boolean;
}

interface ArrayAccess {
    function offsetExists($offset: any): boolean;
    function offsetGet($offset: any): any;
    function offsetSet($offset: any, value: any);
    function offsetUnset($offset: any);
}

class IteratorIterator implements Iterator {
    function __construct($iterator: Traversable);
    function getInnerIterator(): Iterator;
}

class FilterIterator extends IteratorIterator {
    function __construct($iterator: Iterator);
    function accept(): boolean;
}

class RecursiveFilterIterator extends FilterIterator  {
    function __construct($iterator: RecursiveIterator);

    function getChildren(): RecursiveFilterIterator;
    function hasChildren(): boolean;
}


class ArrayIterator implements Iterator {
    function seek($position: number);
    function offsetExists($offset: any): boolean;
    function offsetGet($offset: any): any;
    function offsetSet($offset: any, value: any);
    function offsetUnset($offset: any);
    function count(): number;
    function serialize(): string;
    function unserialize($serialized: string);
}

class RecursiveIteratorIterator implements ArrayIterator {
//    static CATCH_GET_CHILD: number;
//    static CHILD_FIRST: number;
//    static LEAVES_ONLY: number;
//    static SELF_FIRST: number;

    function __construct($iterator: Iterator, $mode?: number, $flags?: number);
    function beginChildren();
    function beginIteration();
    function callGetChildren(): RecursiveIterator;
    function callHasChildren(): boolean;
    function endChildren();
    function endIteration();
    function getDepth(): number;
    function getMaxDepth(): number;
    function getSubIterator(): RecursiveIterator;
    function nextElement();
    function setMaxDepth($max_depth?: number); //doc says string, I'm ignoring the doc

    function current(): any;
    function key(): any;
    function next();
    function rewind();
    function valid(): bool;
    function getInnerIterator(): Iterator;
}

/*
class AppendIterator extends IteratorIterator {
    function __construct();
    function append($iterator: Iterator);
    function getArrayIterator(): ArrayIterator;
    function getIteratorIndex(): $number;
}


/*

interface Countable {
    count(): number;
}

interface SeekableIterator extends Iterator {
    seek(position: number);
}

interface RecursiveIterator extends Iterator {
    getChildren(): RecursiveIterator;
    hasChildren(): bool;
}

interface OuterIterator extends Iterator {
    getInnerIterator(): Iterator;
}

interface SplObserver {
    update(subject: SplSubject);
}

interface SplSubject {
    attach(observer: SplObserver);
    detach(observer: SplObserver);
    notify();
}

class SplDoublyLinkedList implements Iterator, ArrayAccess, Countable {
    static IT_MODE_DELETE: number;
    static IT_MODE_FIFO: number;
    static IT_MODE_KEEP: number;
    static IT_MODE_LIFO: number;

    bottom(): any;
    getIteratorMode(): number;
    isEmpty(): bool;
    pop(): bool;
    prev();
    push(value: any);
    serialize(): string;
    setIteratorMode(mode: number);
    shift(): any;
    top(): any;
    unserialize(serialized: string);
    unshift(value: any);

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
    count(): number;
}

class SplStack extends SplDoublyLinkedList {

}

class SplQueue extends SplDoublyLinkedList {
    dequeue(): any;
    enqueue(value: any);
}

class SplHeap implements Iterator, Countable {
    compare(value1: any, value2: any): number;
    extract(): any;
    insert(value: any);
    isEmpty(): bool;
    recoverFromCorruption();
    top(): any;

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    count(): number;
}

class SplMaxHeap extends SplHeap {
}

class SplMinHeap extends SplHeap {
}

class SplPriorityQueue implements Iterator, Countable {
    static EXTR_BOTH: number;
    static EXTR_DATA: number;
    static EXTR_PRIORITY: number;

    compare(priority1: any, priority2: any): number;
    extract(): any;
    insert(value: any, priority: any);
    isEmpty(): bool;
    recoverFromCorruption();
    setExtractFlags(flags: number);
    top(): any;

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    count(): number;
}

class SplFixedArray implements Iterator, ArrayAccess, Countable {
    static fromArray(array: Array, save_indexes?: bool): SplFixedArray;

    __construct(size?: number);
    getSize(): number;
    setSize(size: number): number;
    toArray(): Array;
    __wakeup();

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
    count(): number;
}

class SplObjectStorage implements Countable, Iterator, Serializable, ArrayAccess {
    addAll(storage: SplObjectStorage);
    attach(object: Object, data?: any);
    contains(object: Object): bool;
    detach(object: Object);
    getHash(object?: Object): string;
    getInfo(): any;
    removeAll(storage: SplObjectStorage);
    removeAllExcept(storage: SplObjectStorage);
    setInfo(data: any);

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
    count(): number;
    serialize(): string;
    unserialize(serialized: string);
}

class ArrayIterator implements ArrayAccess, SeekableIterator, Countable, Serializable {
    __construct(array?: any, flags?: number);
    append(value: any);
    asort();
    getArrayCopy(): Array;
    getFlags(); //TODO: what?
    ksort();
    natcasesort();
    natsort();
    setFlags(flags: string); //TODO: string?
    uasort(cmp_function: string); //TODO: string?
    uksort(cmp_function: string); //TODO: string?

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    seek(position: number);
    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
    count(): number;
    serialize(): string;
    unserialize(serialized: string);
}

class RecursiveArrayIterator extends ArrayIterator implements RecursiveIterator {
    getChildren(): RecursiveIterator;
    hasChildren(): bool;
}

class EmptyIterator implements Iterator {
    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
}

class CachingIterator extends IteratorIterator implements ArrayAccess, Countable {
    static CALL_TOSTRING: number;
    static CATCH_GET_CHILD: number;
    static FULL_CACHE: number;
    static TOSTRING_USE_CURRENT: number;
    static TOSTRING_USE_INNER: number;
    static TOSTRING_USE_KEY: number;

    __construct(iterator: Iterator, flags?: number);
    getCache(); //TODO: wat?
    getFlags(); //TODO: wat?
    setFlags(flags: number);

    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
    count(): number;
}

class RecursiveCachingIterator extends CachingIterator implements RecursiveIterator {
    getChildren(): RecursiveCachingIterator;
    hasChildren(): bool;
}

class CallbackFilterIterator extends FilterIterator {
    __construct(iterator: Iterator, callback: (current: any, key: any, iterator: Iterator) => bool);
}

class RecursiveCallbackFilterIterator extends CallbackFilterIterator implements RecursiveIterator {
    __construct(iterator: RecursiveIterator, callback: (current: any, key: any, iterator: Iterator) => bool);

    getChildren(): RecursiveCallbackFilterIterator;
    hasChildren(): bool;
}

class ParentIterator extends RecursiveFilterIterator {
    getChildren(): ParentIterator;
}

class RegexIterator extends FilterIterator {
    static ALL_MATCHES: number;
    static GET_MATCH: number;
    static MATCH: number;
    static REPLACE: number;
    static SPLIT: number;
    static USE_KEY: number;

    __construct(iterator: Iterator, regex: string, mode?: number, flags?: number, preg_flags?: number);
    getFlags(): number;
    getMode(): number;
    getPregFlags(): number;
    getRegex(): string;
    setFlags(flags: number);
    setMode(mode: number);
    setPregFlags(pref_flags: number);
}

class RecursiveRegexIterator extends RegexIterator implements RecursiveIterator {
    __construct(iterator: RecursiveIterator, regex: string, mode?: number, flags?: number, preg_flags?: number);

    getChildren(): RecursiveIterator;
    hasChildren(): bool;
}

class InfiniteIterator extends IteratorIterator {
    __construct(iterator: Iterator);
}

class LimitIterator extends IteratorIterator {
    __construct(iterator: Iterator, offset?: number, count?: number);
    getPosition(): number;
    seek(position: number): number;
}

class NoRewindIterator extends IteratorIterator {
    __construct(iterator: Iterator);
}

class MultipleIterator implements Iterator {
    static MIT_KEYS_ASSOC: number;
    static MIT_KEYS_NUMERIC: number;
    static MIT_NEED_ALL: number;
    static MIT_NEED_ANY: number;

    __construct(flags?: number);
    attachIterator(iterator: Iterator, infos?: string);
    containsIterator(iterator: Iterator); //TODO: wat?
    countIterators(); //TODO: wat?
    detachIterator(iterator: Iterator);
    getFlags(); //TODO: wat?
    setFlags(flags: number);

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
}

class RecursiveTreeIterator extends RecursiveIteratorIterator {
    static BYPASS_CURRENT: number;
    static BYPASS_KEY: number;
    static PREFIX_END_LAST: number;
    static PREFIX_END_HAS_NEXT: number;
    static PREFIX_LEFT: number;
    static PREFIX_MID_HAS_NEXT: number;
    static PREFIX_MID_LAST: number;
    static PREFIX_RIGHT: number;

    __construct(it: RecursiveIterator, flags?: number, cit_flags?: number, mode?: number);
    __construct(it: IteratorAggregate, flags?: number, cit_flags?: number, mode?: number);
    getEntry(): string;
    getPostfix(); //TODO: wat?
    getPrefix(): string;
    setPrefixPart(part: number, value: string);
}

class LogicException extends Exception { }

class BadFunctionCallException extends LogicException { }

class BadMethodCallException extends BadFunctionCallException { }

class DomainException extends LogicException { }

class InvalidArgumentException extends LogicException { }

class LengthException extends LogicException { }

class OutOfRangeException extends LogicException { }

class RuntimeException extends Exception { }

class OutOfBoundsException extends RuntimeException { }

class OverflowException extends RuntimeException { }

class RangeException extends RuntimeException { }

class UnderflowException extends RuntimeException { }

class UnexpectedValueException extends RuntimeException { }
*/

class SplFileInfo {
    function __construct($file_name: string);
    function getATime(): number;
    function getBasename($suffix?: string): string;
    function getCTime(): number;
    function getExtension(): string;
    function getFileInfo($class_name?: string): SplFileInfo;
    function getFilename(): string;
    function getGroup(): number;
    function getInode(): number;
    function getLinkTarget(): string;
    function getMTime(): number;
    function getOwner(): number;
    function getPath(): string;
    function getPathInfo($class_name?: string): SplFileInfo;
    function getPathname(): string;
    function getPerms(): number;
    function getRealPath(): string;
    function getSize(): number;
    function getType(): string;
    function isDir(): bool;
    function isExecutable(): bool;
    function isFile(): bool;
    function isLink(): bool;
    function isReadable(): bool;
    function isWritable(): bool;
    //openFile($open_mode?: string, use_include_path?: bool, context?: Pct.PhpResource): SplFileObject;
    function setFileClass($class_name?: string);
    function setInfoClass($class_name?: string);
}

/*
class SplFileObject extends SplFileInfo implements RecursiveIterator, SeekableIterator {
    static DROP_NEW_LINE: number;
    static READ_AHEAD: number;
    static READ_CSV: number;
    static SKIP_EMPTY: number;

    __construct(filename: string, open_mode?: string, use_include_path?: bool, context?: Pct.PhpResource);
    eof(): bool;
    fflush(): bool;
    fgetc(): string;
    fgetcsv(delimiter?: string, enclosure?: string, escape?: string): any[];
    fgets(): string;
    fgetss(allowable_tags?: string): string;
    flock(operation: number, $wouldblock?: number): bool;
    fpassthru(): number;
    fputcsv(fields: any[], delimiter?: string, enclosure?: string): number;
    fscanf(format?: string): any[]; //NOTE: the assign-variables version does not work currently
    fseek(offset: number, whence?: number): number;
    fstat(): Pct.PhpAssocArray;
    ftell(): number;
    ftruncate(size: number): bool;
    fwrite(str: string, length?: number): number;
    getCsvControl(): string[];
    getFlags(): number;
    getMaxLineLen(): number;
    setCsvControl(delimiter?: string, enclosure?: string, escape?: string);
    setFlags(flags: number);
    setMaxLineLen(max_len: number);

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    getChildren(): RecursiveIterator;
    hasChildren(): bool;
    seek(position: number);
}

class SplTempFileObject extends SplFileObject {
    __construct(max_memory?: number);
}

class DirectoryIterator extends SplFileInfo implements SeekableIterator {
    isDot(): bool;

    current(): any;
    key(): any;
    next();
    rewind();
    valid(): bool;
    seek(position: number);
}

class FilesystemIterator extends DirectoryIterator {
    static CURRENT_AS_FILEINFO: number;
    static CURRENT_AS_PATHNAME: number;
    static CURRENT_AS_SELF: number;
    static CURRENT_MODE_MASK: number;
    static FOLLOW_SYMLINKS: number;
    static KEY_AS_FILENAME: number;
    static KEY_AS_PATHNAME: number;
    static KEY_MODE_MASK: number;
    static NEW_CURRENT_AND_KEY: number;
    static SKIP_DOTS: number;
    static UNIX_PATHS: number;

    __construct(path: string, flags?: number);
}

class GlobIterator extends FilesystemIterator implements Countable {
    count(): number;
} */

class RecursiveDirectoryIterator extends Iterator /*FilesystemIterator implements RecursiveIterator*/ {
    function getSubPath(): string;
    function getSubPathname(): string;

    function getChildren(): RecursiveIterator;
    function hasChildren($allow_links?: bool): bool;
}

/*
class ArrayObject implements IteratorAggregate, ArrayAccess, Serializable, Countable {
    static ARRAY_AS_PROPS: number;
    static STD_PROP_LIST: number;

    __construct(input?: any, flags?: number, iterator_class?: string);
    append(value: any);
    asort();
    exchangeArray(input?: any): Array;
    getArrayCopy(): Array;
    getFlags(): number;
    getIteratorClass(): string;
    ksort();
    natcasesort();
    natsort();
    setFlags(flags: number);
    setIteratorClass(iterator_class: string);
    uasort(cmp_function: (a: any, b: any) => number);
    uksort(cmp_function: (a: any, b: any) => number);

    getIterator(): ArrayIterator;
    offsetExists(offset: any): bool;
    offsetGet(offset: any): any;
    offsetSet(offset: any, value: any);
    offsetUnset(offset: any);
    serialize(): string;
    unserialize(serialized: string);
    count(): number;
}*/

//function class_implements(class_: any, autoload?: bool): Pct.PhpAssocArray;
//function class_parents(class_: any, autoload?: bool): Pct.PhpAssocArray;
//function class_uses(class_: any, autoload?: bool): Pct.PhpAssocArray;
//function iterator_apply(iterator: Traversable, function_: (iterator: Traversable) => any, args?: any[]): number;
//function iterator_count(iterator: Traversable): number;
//function iterator_to_array(iterator: Traversable): any[];
//function iterator_to_array(iterator: Traversable, use_keys: bool): Array;
//function spl_autoload(class_name: string, file_extensions?: string);
//function spl_autoload_call(class_name: string);
//function spl_autoload_extensions(file_extensions?: string): string;
function spl_autoload_functions(): array<any>;
function spl_autoload_register($autoload_function?: any, $throw_?: boolean, $prepend?: boolean): boolean;
//function spl_autoload_unregister(autoload_function: (class_: string) => void): bool;
//function spl_classes(): Pct.PhpAssocArray;
//function spl_object_hash(obj: any): string;
