
const LIBXML_COMPACT: number;
const LIBXML_DOTTED_VERSION: number;
const LIBXML_DTDATTR: number;
const LIBXML_DTDLOAD: number;
const LIBXML_DTDVALID: number;
const LIBXML_ERR_ERROR: number;
const LIBXML_ERR_FATAL: number;
const LIBXML_ERR_NONE: number;
const LIBXML_ERR_WARNING: number;
const LIBXML_NOBLANKS: number;
const LIBXML_NOCDATA: number;
const LIBXML_NOEMPTYTAG: number;
const LIBXML_NOENT: number;
const LIBXML_NOERROR: number;
const LIBXML_NONET: number;
const LIBXML_NOWARNING: number;
const LIBXML_NOXMLDECL: number;
const LIBXML_NSCLEAN: number;
const LIBXML_PARSEHUGE: number;
const LIBXML_VERSION: number;
const LIBXML_XINCLUDE: number;

class LibXMLError {
    level: number;
    code: number;
    column: number;
    message: string;
    file: string;
    line: number;
}

function libxml_clear_errors();
function libxml_disable_entity_loader(disable?: bool): bool;
function libxml_get_errors(): LibXMLError[];
function libxml_get_last_error(): LibXMLError;
function libxml_set_external_entity_loader(resolver_function: (publicId: string, systemId: string, context: Pct.PhpAssocArray) => any);
function libxml_set_streams_context(streams_context: Pct.PhpResource);
function libxml_use_internal_errors(use_errors?: bool): bool;