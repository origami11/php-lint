
const FILE_APPEND: number;
const FILE_BINARY: number;
const FILE_IGNORE_NEW_LINES: number;
const FILE_NO_DEFAULT_CONTEXT: number;
const FILE_SKIP_EMPTY_LINES: number;
const FILE_USE_INCLUDE_PATH: number;

const GLOB_AVAILABLE_FLAGS: number;
const GLOB_BRACE: number;
const GLOB_MARK: number;
const GLOB_NOCHECK: number;
const GLOB_NOESCAPE: number;
const GLOB_NOSORT: number;
const GLOB_ONLYDIR: number;

const LOCK_EX: number;
const LOCK_NB: number;
const LOCK_SH: number;
const LOCK_UN: number;

const PATHINFO_BASENAME: number;
const PATHINFO_DIRNAME: number;
const PATHINFO_EXTENSION: number;
const PATHINFO_FILENAME: number;

const SEEK_CUR: number;
const SEEK_END: number;
const SEEK_SET: number;

class PhpResource {
}

function basename($path: string, $suffix?: string): string;
//function chgrp(filename: string, group: string): boolean;
//function chgrp(filename: string, group: number): boolean;
//function chmod(filename: string, mode: number): boolean;
//function chown(filename: string, user: string): boolean;
//function chown(filename: string, user: number): boolean;
//function clearstatcache(clear_realpath_cache?: bool, filename?: string);
function copy($source: string, $dest: string, $context?: PhpResource): boolean;
//function dirname(path: string): string;
//function disk_free_space(directory: string): number;
//function disk_total_space(directory: string): number;
function fclose($handle: PhpResource): boolean;
function feof($handle: PhpResource): boolean;
//function fflush(handle: Pct.PhpResource): boolean;
//function fgetc(handle: Pct.PhpResource): string;
function fgetcsv($handle: PhpResource, $length?: number, $delimter?: string, $enclosure?: string, $escape?: string): array<any>;
function fgets($handle: PhpResource, $length?: number): string;
//function fgetss(handle: Pct.PhpResource, length?: number, allowable_tags?: string): string;
function file($filename: string, $flags?: number, $context?: PhpResource): array<string>;
//function file_exists(filename: string): boolean;
//function file_get_contents(filename: string, use_include_path?: bool, context?: Pct.PhpResource, offset?: number, maxlen?: number): string;
//function file_put_contents(filename: string, data: string, flags?: number, context?: Pct.PhpResource): number;
//function file_put_contents(filename: string, data: any[], flags?: number, context?: Pct.PhpResource): number;
//function file_put_contents(filename: string, data: Pct.PhpResource, flags?: number, context?: Pct.PhpResource): number;
//function fileatime(filename: string): number;
//function filectime(filename: string): number;
//function filegroup(filename: string): number;
//function fileinode(filename: string): number;
//function filemtime(filename: string): number;
//function fileowner(filename: string): number;
//function fileperms(filename: string): number;
//function filesize(filename: string): number;
//function filetype(filename: string): string;
//function flock(handle: Pct.PhpResource, operation: number, $wouldblock?: number): boolean;
//function fnmatch(pattern: string, string_: string, flags?: number): boolean;
function fopen($filename: string, $mode: string, $use_include_path?: boolean, $context?: PhpResource): PhpResource | boolean;
//function fpassthru(handle: Pct.PhpResource): number;
function fputcsv($handle: PhpResource, $fields: array<any>, $delimiter?: string, $enclosure?: string): number;
function fread($handle: PhpResource, $length: number): string;
//function fscanf(handle: Pct.PhpResource, format: string): any[]; //NOTE: auto-assignment not available
function fseek($handle: PhpResource, $offset: number, $whence?: number): number;
//function fstat(handle: Pct.PhpResource): Pct.PhpAssocArray;
//function ftell(handle: Pct.PhpResource): number;
//function ftruncate(handle: Pct.PhpResource, size: number): boolean;
function fwrite($handle: PhpResource, $string_: string, $length?: number): number;
function fputs($handle: PhpResource, $string_: string, $length?: number): number;

//function glob(pattern: string, flags?: number): string[];
//function is_dir(filename: string): boolean;
//function is_executable(filename: string): boolean;
//function is_file(filename: string): boolean;
//function is_link(filename: string): boolean;
function is_readable(filename: string): boolean;
function is_uploaded_file($filename: string): boolean;
function is_writable($filename: string): boolean;
//function lchgrp(filename: string, group: string): boolean;
//function lchgrp(filename: string, group: number): boolean;
//function lchown(filename: string, user: string): boolean;
//function lchown(filename: string, user: number): boolean;
//function link(target: string, link: string): boolean;
//function linkinfo(path: string): number;
//function lstat(filename: string): Pct.PhpAssocArray;
function mkdir($pathname: string, $mode?: number, $recursive?: boolean, context?: PhpResource): boolean;
function move_uploaded_file($filename: string, $destination: string): boolean;
//function parse_ini_file(filename: string, process_sections?: bool, scanner_mode?: number): Pct.PhpAssocArray;
//function parse_ini_string(ini: string, process_sections?: bool, scanner_mode?: number): Pct.PhpAssocArray;
//function pathinfo(path: string): Pct.PhpAssocArray;
//function pathinfo(path: string, options: number): any;

function pathinfo ($path: string, $options: number): string;
function pathinfo ($path: string): array<string>;

function pclose($handle: PhpResource): number;
function popen($command: string, $mode: string): PhpResource;
function readfile($filename: string, $use_include_path?: boolean, $context?: PhpResource): number;
function readlink($path: string): string;
function realpath($path: string): string;
//function realpath_cache_get(): Pct.PhpAssocArray;
function realpath_cache_size(): number;
function rename($oldname: string, $newname: string, $context?: PhpResource): boolean;
function rewind($handle: PhpResource): boolean;
function rmdir($dirname: string, $context?: PhpResource): boolean;
//function stat(filename: string): Pct.PhpAssocArray;
function symlink($target: string, $link: string): boolean;
function tempnam($dir: string, $prefix: string): string;
function tmpfile(): PhpResource;
function touch($filename: string, $time?: number, $atime?: number): boolean;
//function umask(umask?: number): number;
function unlink(filename: string): boolean;
