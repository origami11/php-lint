﻿
class PDOStatement extends Iterator {
    function execute($input_parameters?: array<string | number>): boolean;
    function fetch($options?: number): array<string | number>;
    function fetchAll($fetch_style?: number, $fetch_argument?: any): array<array<string | number>>;
    function bindValue ($parameter: string, $value: any, $data_type?: number): boolean;
    function setFetchMode($options?: number, $fetch_argument?: any);
}

class PDO<PDOStatement> {
    var ATTR_ERRMODE: number;
    var ATTR_DEFAULT_FETCH_MODE: number;

    var ERRMODE_EXCEPTION: number;
    var FETCH_ASSOC: number;
    var FETCH_CLASS: number;
    var PARAM_LOB: number;
    var PARAM_STR: number;
    var PARAM_INT: number;
    var ATTR_TIMEOUT: number;
    var ATTR_STATEMENT_CLASS: number;        

    function __construct($path: string, $user?: string, $password?: string);
    function prepare($statement: string<valid>, $options?: any): PDOStatement;
    function query($statement: string<valid>): PDOStatement;
    function exec($statement?: string<valid>): number;
    function setAttribute($key: number, $value: any);
    function sqliteCreateFunction($name: string, $fn: any, $args: number);

    function beginTransaction();
    function commit();
    function quote($s: string): string<valid>;
    function lastInsertId(): number;
}

class PDOException extends Exception {
    var errorInfo: array<any>;
}