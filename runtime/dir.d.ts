
const DIRECTORY_SEPARATOR: string;
const PATH_SEPARATOR: string;

const SCANDIR_SORT_ASCENDING: number;
const SCANDIR_SORT_DESCENDING: number;
const SCANDIR_SORT_NONE: number;

class Directory {
    var path: string;
    var handle: PhpResource;

    function close($dir_handle?: PhpResource);
    function read($dir_handle?: PhpResource): string;
    function rewind($dir_handle?: PhpResource);
}

function chdir($directory: string): boolean;
function chroot($directory: string): boolean;
function closedir($dir_handle?: PhpResource);
function dir($directory: string, $context?: PhpResource): Directory;
function getcwd(): string;
function opendir($path: string, $context?: PhpResource): PhpResource;
function readdir($dir_handle?: PhpResource): string | boolean;
function rewinddir($dir_handle?: PhpResource);
function scandir($directory?: string, $sorting_order?: number, $context?: PhpResource): array<string>;
