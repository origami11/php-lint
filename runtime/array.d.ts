
const CASE_LOWER: number;
const CASE_UPPER: number;

const SORT_ASC: number;
const SORT_DESC: number;
const SORT_REGULAR: number;
const SORT_NUMERIC: number;
const SORT_STRING: number;
const SORT_LOCALE_STRING: number;
const SORT_NATURAL: number;
const SORT_FLAG_CASE: number;

const COUNT_NORMAL: number;
const COUNT_RECURSIVE: number;

function count($array: array<any> | ArrayIterator): number;
function sizeof($array: array<any> | ArrayIterator): number;
function reset($array: array<any> | Iterator): any;
function key($array: array<any>): any;
function next($array: array<any> | Iterator): any;
function current($array: array<any>): any;
function end($array: array<any>): any;
function range($start: number, $end: number): array<number>;

function array_map($callback: any, $array: array<any>, $array1?: array<any>, $array2?: array<any>): array<any>;
function array_keys($data: array<any>): array<string>;
function array_values($data: array<any>): array<any>;

function in_array($x: any,  $haystack: array<any>): boolean;
function array_merge($arr1: array<any>, $arr2?: array<any>, $arr3?: array<any>): array<any>;
function array_filter($arry: array<any>, $callback: any): array<any>;
function array_search($needle: any, $haystack: array<any>): any;
function iterator_to_array($value: Iterator): array<any>;
function array_to_object($x: any, $y: array<any>);
function array_reverse($array: array<any>, $preserve_keys?: boolean): array<any>;
function ksort($arr: array<any>);
function getdate($timestamp: number): array<number|string>;

function array_splice($input: array<any>, $offset: number , $length?: number, $replacement?: array<any>): array<any>;
function array_slice($input: array<any>, $offset: number , $length?: number, $preserve_keys?: boolean): array<any>;

function array_push($array: array<any>, $value1: any, $value2?: any, $value3?: any, $value4?: any, $value5?: any, $value6?: any): number;
function array_flip($array: array<any>): array<any>;
function array_shift($array: array<any>): any;
function array_unshift($array: array<any>, $value?: any): number;
function array_pop($array: array<any>): any;

function array_diff($array1: array<any>, $array2: array<any>, $array3?: array<any>, $array4?: array<any>, $array5?: array<any>): array<any>;
function array_unique($data: array<any>, $sort_flags?: number): array<any>;

function usort($array: array<any>, $value_compare_func: any): boolean;
function array_key_exists ($key: string, $array:array<any>): boolean;
function shuffle($arr: array<any>): boolean;

function array_rand($array: array<any>): string;
function array_rand($array: array<any>, $num: number): array<string>;
