﻿//var reader = require('php-parser');
//reader.parser.locations = false;
//reader.parser.debug = true;
const fs = require('fs');
const parser = require('php-parser');
const path = require('path');
const glob = require('glob');

var reader = new parser({
    parser: {
        extractDoc: false,
        suppressErrors: true,
        php7: true
    },
    ast: {
        withPositions: true
    },
    lexer: {
        short_tags: true,
        asp_tags: true
    }
});

function isObject(o) {
  return o instanceof Object && typeof o == 'object';
}

function walk(ast, cc) {
    if (Array.isArray(ast)) {       
        ast.forEach(item => walk(item, cc));
    } else if (isObject(ast)) {
        cc(ast);
        for(var i in ast) {
            if (i != 'kind' && i != 'loc' && ast.hasOwnProperty(i)) {
                walk(ast[i], cc);
            }
        }
    }
}

function findAfter(patches, offset) {
    var result = [];
    for(var i = 0; i < patches.length; i++) {   
        var item = patches[i];
        if (item.type == 'after' && item.after == offset) {
            result.push(item);
        }
    }
    return result;
}

function findBefore(patches, offset) {
    var result = [];
    for(var i = 0; i < patches.length; i++) {   
        var item = patches[i];
        if (item.type == 'before' && item.before == offset) {
            result.push(item);
        }
    }
    return result;
}

function findReplace(patches, offset, val) {
    var result = [];
    for(var i = 0; i < patches.length; i++) {   
        var item = patches[i];
        if (item.type == 'replace' && item.after <= offset && offset <= item.before && val == item.from) {
            result.push(item);
        }
    }
    return result;
}

function printWithPatches(patches, tokens) {
    var output = [], offset = 0;
    for(var i = 0; i < tokens.length; i++) {
        var item = tokens[i];
        var val = Array.isArray(item) ? item[1] : item;

        var patch = findBefore(patches, offset);
        if (patch.length > 0) {
            patch.forEach(n => output.push(n.ast));
        }

        var patch = findReplace(patches, offset, val);
        if (patch.length > 0) {
            patch.forEach(n => output.push(n.ast));
        } else {
            output.push(val);
        }
        offset += val.length;

        var patch = findAfter(patches, offset);
        if (patch.length > 0) {
            patch.forEach(n => output.push(n.ast));
        }
    }
    return output;
}

function fixNamespace(file) {
    var code = fs.readFileSync(file, 'utf-8');  
    var ast = reader.parseCode(code);
    var tokens = parser.tokenGetAll(code);

    var patches = []
    var uses = [];
    var writeNs = true;        
    var nsPos = null;
    var locals = [];

    walk(ast, function (node) {
        if (node.kind == 'constref') {
            var ref = node.name;
            if (ref.name == 'ADMIN_PATH') {
                patches.push({
                    type: 'replace',
                    after: ref.loc.start.offset,
                    before: ref.loc.end.offset,
                    from: ref.name,
                    ast: "'admin'"
                });
            }
            if (ref.name == 'BASE_PATH') {
                patches.push({
                    type: 'replace',
                    after: ref.loc.start.offset,
                    before: ref.loc.end.offset,
                    from: ref.name,
                    ast: "$this->config->get('site', 'path')"
                });
            }
            if (ref.name == 'CMS_PATH') {
                patches.push({
                    type: 'replace',
                    after: ref.loc.start.offset,
                    before: ref.loc.end.offset,
                    from: ref.name,
                    ast: "$this->config->get('system', 'path')"
                });
            }
            if (ref.name == 'WWW_PATH') {
                patches.push({
                    type: 'replace',
                    after: ref.loc.start.offset,
                    before: ref.loc.end.offset,
                    from: ref.name,
                    ast: "$this->config->get('system', 'web')"
                });
                
            }
            if (ref.name == 'SITE_WWW_PATH') {
                patches.push({
                    type: 'replace',
                    after: ref.loc.start.offset,
                    before: ref.loc.end.offset,
                    from: ref.name,
                    ast: "$this->config->get('site', 'web')"
                });
            }
            if (ref.name == 'CMS_VERSION') {
                patches.push({
                    type: 'replace',
                    after: ref.loc.start.offset,
                    before: ref.loc.end.offset,
                    from: ref.name,
                    ast: "$this->config->get('system', 'version')"
                });
            }
        }
    });


    var out = printWithPatches(patches, tokens);
//    fs.writeFileSync(file + '.bak', out.join(''));
    fs.writeFileSync(file, out.join(''));
}

var globals = fs.readFileSync('global.txt', 'utf-8').split('\n').map(x => x.trim());

//var basePath = 'D:/Server/www/projects/cms_master/public/vendor/ctiso/core/src/';
var basePath = 'D:/Server/www/projects/cms_master/public/application/modules';
//var basePath = 'D:/Server/www/projects/cms_master/public/components';
//var basePath = 'D:/Server/www/projects/cms_master/public/vendor/ctiso/core'
//var basePath = './';
var listPath = basePath + '/**/*.php';
glob(listPath, function (er, files) {    
    files.forEach(fixNamespace);
});


