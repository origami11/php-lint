﻿//var reader = require('php-parser');
//reader.parser.locations = false;
//reader.parser.debug = true;
var fs = require('fs');

var parser = require('php-parser');
var code = fs.readFileSync('sample/Logger.php', 'utf-8');
var tokens = parser.tokenGetAll(code);

function fixStyle(tokens) {
    var output = [];
    var prev = null;
    for(var i = 0; i < tokens.length; i++) {
        var t = tokens[i];
        var type = Array.isArray(t) ? t[0] : t;
        
        if (prev != 'T_WHITESPACE' && type == '{') {
            output.push(' ');
            }
    
        if (prev == ',' && type != 'T_WHITESPACE') {
            output.push(' ');
        }
        
        if (Array.isArray(t)) {
                output.push(t[1]);
            prev = t[0];
        } else {
            output.push(t);
            prev = t;
        }
    }       
    return output;
}

console.log(out.join(''));


