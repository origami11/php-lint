﻿//var reader = require('php-parser');
//reader.parser.locations = false;
//reader.parser.debug = true;
const fs = require('fs');
const parser = require('php-parser');
const path = require('path');
const glob = require('glob');

var reader = new parser({
    parser: {
        extractDoc: false,
        suppressErrors: true,
        php7: true
    },
    ast: {
        withPositions: true
    },
    lexer: {
        short_tags: true,
        asp_tags: true
    }
});

function isObject(o) {
  return o instanceof Object && typeof o == 'object';
}

function walk(ast, cc) {
    if (Array.isArray(ast)) {       
        ast.forEach(item => walk(item, cc));
    } else if (isObject(ast)) {
        cc(ast);
        for(var i in ast) {
            if (i != 'kind' && i != 'loc' && ast.hasOwnProperty(i)) {
                walk(ast[i], cc);
            }
        }
    }
}

function findAfter(patches, offset) {
    var result = [];
    for(var i = 0; i < patches.length; i++) {   
        var item = patches[i];
        if (item.type == 'after' && item.after == offset) {
            result.push(item);
        }
    }
    return result;
}

function findBefore(patches, offset) {
    var result = [];
    for(var i = 0; i < patches.length; i++) {   
        var item = patches[i];
        if (item.type == 'before' && item.before == offset) {
            result.push(item);
        }
    }
    return result;
}

function findReplace(patches, offset, val) {
    var result = [];
    for(var i = 0; i < patches.length; i++) {   
        var item = patches[i];
        if (item.type == 'replace' && item.after <= offset && offset <= item.before && val == item.from) {
            result.push(item);
        }
    }
    return result;
}

function printWithPatches(patches, tokens) {
    var output = [], offset = 0;
    for(var i = 0; i < tokens.length; i++) {
        var item = tokens[i];
        var val = Array.isArray(item) ? item[1] : item;

        var patch = findBefore(patches, offset);
        if (patch.length > 0) {
            patch.forEach(n => output.push(n.ast));
        }

        var patch = findReplace(patches, offset, val);
        if (patch.length > 0) {
            patch.forEach(n => output.push(n.ast));
        } else {
            output.push(val);
        }
        offset += val.length;

        var patch = findAfter(patches, offset);
        if (patch.length > 0) {
            patch.forEach(n => output.push(n.ast));
        }
    }
    return output;
}

var superglobal = ['self', 'parent', '\\array'];
function fixName(node, uses, patches, locals) {
    var name = node.name;
    if (!uses.find(x => x[0] == name) && superglobal.indexOf(name) < 0 && locals.indexOf(name) < 0) {
        uses.push([name, globals.indexOf(name) >= 0]);
    }            
    if (globals.indexOf(name) < 0) {
        var shortname = name.split('_');
        patches.push({
            type: 'replace',
            after: node.loc.start.offset,
            before: node.loc.end.offset,
            from: name,
            ast: shortname[shortname.length - 1]
        });
    }
}

function fixNamespace(file) {
    var code = fs.readFileSync(file, 'utf-8');  
    var ast = reader.parseCode(code);
    var tokens = parser.tokenGetAll(code);

    var patches = []
    var uses = [];
    var writeNs = true;        
    var nsPos = null;
    var locals = [];

    walk(ast, function (node) {
        if (node.kind == 'new') {
            if (node.what.kind == 'identifier') {
                fixName(node.what, uses, patches, locals);
            }           
        }
        if (node.kind == 'staticlookup') {
            if (node.what.kind == 'identifier') {
                fixName(node.what, uses, patches, locals);
            }
        }
        if (node.kind == 'class') {
            locals.push(node.name);

            var altname = node.name.split('_');
            var c = node.body;
            patches.push({
                type: 'replace',
                after: node.loc.start.offset,
                before: c.length > 0 ? c[0].loc.start.offset : node.loc.end.offset,
                from: node.name,
                ast: altname[altname.length - 1]            
            });

            if (node.extends) {
                var ext = node.extends;
                if (ext.kind == 'identifier') {
                    fixName(ext, uses, patches, locals);
                }
            }
        }
        if (node.kind == 'namespace') {
            writeNs = false;
            nsPos = node.children[0].loc.start.offset;
        }
        if (node.kind == 'parameter') {
            if (node.type && node.type.kind == 'identifier') {
                var name = node.type.name;
                if (uses.indexOf(name) < 0 && globals.indexOf(name) < 0) {
                    fixName(node.type, uses, patches, locals);
                }
            }
        }
    });

    var nsPrefix = 'App';

    var rel = path.dirname(path.relative(basePath, file));
    if (rel != '.') {
        nsName = nsPrefix + '\\' + rel;
    } else {
        nsName = nsPrefix;
    }

    if (writeNs) {
        patches.push({
            before: ast.children[0].loc.start.offset,
            type: 'before',
            ast: 'namespace ' + nsName + ';\n' //node('namespace', {});
        });
    }
    
    if (uses.length > 0) {
        var code = 'use ' + uses.map(x => {            
                return x[1] ? x[0] : 'ctiso' + '\\' + x[0].split('_').join('\\');
            }).join(",\n    ") + ';\n';

        if (nsPos) {
            patches.push({type: 'before', before: nsPos, ast: code});
        } else {
            patches.push({type: 'before', before: ast.children[0].loc.start.offset, ast: code});
        }
    }

    patches.push({
        type: 'before',
        before: ast.children[0].loc.start.offset,
        ast: '\n'
    });

    var out = printWithPatches(patches, tokens);
//    fs.writeFileSync(file + '.bak', out.join(''));
    fs.writeFileSync(file, out.join(''));
}

var globals = fs.readFileSync('global.txt', 'utf-8').split('\n').map(x => x.trim());

//var basePath = 'D:/Server/www/projects/cms_master/public/vendor/ctiso/core/src/';
var basePath = 'D:/Server/www/projects/cms_master/public/application/library'
//var basePath = './';
var listPath = basePath + '/**/*.php';
glob(listPath, function (er, files) {    
    files.forEach(fixNamespace);
});


